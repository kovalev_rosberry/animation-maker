//
//  main.m
//  AnimationMakerExample
//
//  Created by Anton on 17.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
