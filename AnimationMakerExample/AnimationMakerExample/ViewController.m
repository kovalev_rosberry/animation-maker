//
//  ViewController.m
//  AnimationMakerExample
//
//  Created by Anton on 17.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "ViewController.h"
#import <KAAnimationParser.h>

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *catImageView;
@property (strong, nonatomic) IBOutlet UIButton *animateButton;
@property (assign, nonatomic) BOOL isAnimating;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)animate:(id)sender {
    if (self.isAnimating) {
        [self.catImageView.layer removeAllAnimations];
    } else {
//        [self simpleAnimate];
        [self simpleAnimateWithPerspective];
//        [self animateWithExtraVars];
    }
    [self setIsAnimating:!self.isAnimating];
    
    UIButton *b = sender;
    if (b) {
        if (self.isAnimating)
            [b setTitle:@"Stop!" forState:UIControlStateNormal];
        else
            [b setTitle:@"Animate!" forState:UIControlStateNormal];
    }
}

- (void)simpleAnimate{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Test" ofType:@"plist"];
    KAAnimation *animation = [KAAnimationParser animationWithFileAtPath:path withExtraVariables:nil forLayer:self.catImageView.layer];
    [animation playAnimation];
    [animation setCompleteBlock:^{
        [self.animateButton setTitle:@"Animate!" forState:UIControlStateNormal];
    }];
}

- (void)animateWithExtraVars{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Anim" ofType:@"plist"];
    NSArray *ev = [KAAnimationParser extraVariablesNamesForAnimationAtPath:path];
    
    NSMutableDictionary *dict = [@{} mutableCopy];
    for (NSString *evName in ev) {
        if ([evName isEqualToString:@"MoveX"]) {
            [dict setObject:@(120) forKey:evName];
        }
    }
    
    KAAnimation *animation = [KAAnimationParser animationWithFileAtPath:path withExtraVariables:[NSDictionary dictionaryWithDictionary:dict] forLayer:self.catImageView.layer];
    [animation setWillStartBlock:^{
        
    }];
    [animation setDidStartBlock:^{
        
    }];
    [animation setCompleteBlock:^{
        
    }];
    [animation playAnimation];
}

- (void)simpleAnimateWithPerspective{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"SomeTest" ofType:@"plist"];
    KAAnimation *animation = [KAAnimationParser animationWithFileAtPath:path withExtraVariables:nil forLayer:self.catImageView.layer];
    [animation playAnimation];
    [animation setCompleteBlock:^{
        [self.animateButton setTitle:@"Animate!" forState:UIControlStateNormal];
    }];
}

@end
