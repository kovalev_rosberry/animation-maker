//
//  KAAnimationParser.m
//  AnimationMaker
//
//  Created by Anton on 06.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

typedef enum AnimationTimingFunctionsType{
    AnimationTimingFunctionsTypeLinear,
    AnimationTimingFunctionsTypeEaseIn,
    AnimationTimingFunctionsTypeEaseOut,
    AnimationTimingFunctionsTypeEaseInEaseOut,
    AnimationTimingFunctionsTypeDefault
}AnimationTimingFunctionsType;

typedef enum FillMode{
    FillModeForwards,
    FillModeBackwards,
    FillModeBoth,
    FillModeRemoved
}FillMode;

#define DEGREES_TO_RADIANS(angle)       ((angle) / 180.0 * M_PI)
#define RADIANS_TO_DEGREES(radians)     ((radians) * (180.0 / M_PI))

#import "KAAnimationParser.h"
#import "KAAnimation_private.h"
#import "KAAnimationParserStrings.h"

@implementation KAAnimationParser

+ (KAAnimation *)animationWithFileAtPath:(NSString *)path withExtraVariables:(NSDictionary *)eVars forLayer:(CALayer *)layer{
    CGFloat delay = 0;
    CAKeyframeAnimation *caAnimation = [KAAnimationParser animationFromPlistAtPath:path forLayer:layer withExtraVariables:eVars delay:&delay];
    KAAnimation *animation = [[KAAnimation alloc] initWithCAKeyFrameAnimation:caAnimation fromFileWithPath:path forLayer:layer withDelay:delay];
    return animation;
}

+ (CAKeyframeAnimation *)animationFromPlistAtPath:(NSString *)path forLayer:(CALayer *)layer withExtraVariables:(NSDictionary *)eVars delay:(CGFloat *)delay{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSAssert([fm fileExistsAtPath:path], @"Animation file should exist.");
    
    NSData* plistData = [NSData dataWithContentsOfFile:path];
    NSError *error;
    NSPropertyListFormat format;
    NSDictionary *plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&format error:&error];
    NSAssert(error == nil, @"Can't read plist. Error: %@", [error localizedDescription]);
//    NSLog(@"plist is %@", plist);
    
    NSString *animationKeyPath = [KAAnimationParser valueForVariable:kAnimationKeyPathKey fromDictionary:plist necessarily:YES];
    NSString *currentValuesType = [KAAnimationParser valuesTypeForAnimationKeyPathKey:animationKeyPath];
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:animationKeyPath];
    
    NSDictionary *keyTimes = [KAAnimationParser valueForVariable:kAnimationKeyTimesKey fromDictionary:plist necessarily:YES];
    NSMutableArray *values = [[NSMutableArray alloc] init];
    NSMutableArray *timingFunctions = [@[] mutableCopy];
    
    NSArray *sortedKeys = [[keyTimes allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSNumber *n1 = (NSNumber *)obj1;
        NSNumber *n2 = (NSNumber *)obj2;
        return [n1 compare:n2];
    }];
    
    for (NSNumber *keyNumber in sortedKeys) {
        NSNumber *kNumber = @([keyNumber doubleValue]);
        id preValue = [KAAnimationParser valueForVariable:[NSString stringWithFormat:@"%@", kNumber] fromDictionary:keyTimes necessarily:NO];
        if ([animationKeyPath isEqualToString:@"transform"]) {
            if (preValue && [preValue isKindOfClass:[NSDictionary class]]) {
                NSDictionary *preValues = (NSDictionary *)preValue;
                NSNumber *timingFunction = [preValues objectForKey:kAnimationTimingFunctionsKey];
                [timingFunctions addObject:[KAAnimationParser timingFunctionForKey:timingFunction]];
                if ([currentValuesType isEqualToString:kTransformValuesTypeKey]) {
                    CATransform3D concatTransform = CATransform3DIdentity;
                    CATransform3D transform = CATransform3DIdentity;
                    NSDictionary *tValues = preValues[kTransformValuesTypeKey];
                    NSUInteger k = 0;
                    for (NSString *key in [tValues allKeys]) {
                        transform = [KAAnimationParser transformFromDictionary:tValues[key] extraVariables:eVars transformKey:key forLayer:layer checkM34FromDictionary:plist];
                        if (k == 0)
                            concatTransform = transform;
                        else
                            concatTransform = CATransform3DConcat(concatTransform, transform);
                        ++k;
                    }
                    [values addObject:[NSValue valueWithCATransform3D:concatTransform]];
                }
            } else {
                [timingFunctions addObject:[KAAnimationParser timingFunctionForKey:nil]];
                [values addObject:[NSValue valueWithCATransform3D:layer.transform]];
            }
        } else if ([animationKeyPath isEqualToString:@"opacity"]) {
            if (preValue && [preValue isKindOfClass:[NSDictionary class]]) {
                NSNumber *timingFunction = [preValue objectForKey:kAnimationTimingFunctionsKey];
                [timingFunctions addObject:[KAAnimationParser timingFunctionForKey:timingFunction]];
                id value = preValue[kOpacityValuesTypeKey];
                if ([value isKindOfClass:[NSDictionary class]]) {
                    NSString *eOpacity = value[@"extra_opacity"];
                    if ([[value allKeys] containsObject:eOpacity])
                        value = eVars[eOpacity];
                    else
                        value = value[@"opacity"];
                } else
                    value = @(layer.opacity);
                [values addObject:value];
            } else {
                [timingFunctions addObject:[KAAnimationParser timingFunctionForKey:nil]];
                [values addObject:@(layer.opacity)];
            }
        }
    }
    
    [timingFunctions removeObjectAtIndex:0];
    
    [animation setValues:values];
    [animation setKeyTimes:sortedKeys];
    [animation setTimingFunctions:timingFunctions];
    
    animation.duration = [[KAAnimationParser valueForVariable:kAnimationDurationKey fromDictionary:plist necessarily:YES] doubleValue];
    *delay = [[KAAnimationParser valueForVariable:kAnimationDelayKey fromDictionary:plist necessarily:YES] doubleValue];
    animation.fillMode = [KAAnimationParser fillModeForKey:[KAAnimationParser valueForVariable:kAnimationFillModeKey fromDictionary:plist necessarily:NO]];
    animation.removedOnCompletion = [[KAAnimationParser valueForVariable:kAnimationRemoveOnCompletionKey fromDictionary:plist necessarily:NO] boolValue];
    if ([[KAAnimationParser valueForVariable:kAnimationRepeatKey fromDictionary:plist necessarily:NO] boolValue]) {
        if ([[KAAnimationParser valueForVariable:kAnimationRepeatForeverKey fromDictionary:plist necessarily:NO] boolValue])
            animation.repeatCount = HUGE_VALF;
        else
            animation.repeatCount = [[KAAnimationParser valueForVariable:kAnimationRepeatCountKey fromDictionary:plist necessarily:NO] floatValue];
        animation.repeatDuration = [[KAAnimationParser valueForVariable:kAnimationRepeatDurationKey fromDictionary:plist necessarily:NO] floatValue];
    } else {
        animation.repeatCount = 0;
        animation.repeatDuration = 0;
    }
    animation.autoreverses = [[KAAnimationParser valueForVariable:kAnimationAutoReverseKey fromDictionary:plist necessarily:NO] boolValue];
    
    return animation;
}

+ (CAMediaTimingFunction *)timingFunctionForKey:(NSNumber *)key{
    NSString *name;
    
    switch ([key integerValue]) {
        case AnimationTimingFunctionsTypeLinear:
            name = kCAMediaTimingFunctionLinear;
            break;
        
        case AnimationTimingFunctionsTypeEaseIn:
            name = kCAMediaTimingFunctionEaseIn;
            break;
            
        case AnimationTimingFunctionsTypeEaseOut:
            name = kCAMediaTimingFunctionEaseOut;
            break;
            
        case AnimationTimingFunctionsTypeEaseInEaseOut:
            name = kCAMediaTimingFunctionEaseInEaseOut;
            break;
            
        case AnimationTimingFunctionsTypeDefault:
            name = kCAMediaTimingFunctionDefault;
            break;
            
        default:
            name = kCAMediaTimingFunctionLinear;
            break;
    }
    
    return [CAMediaTimingFunction functionWithName:name];
}

+ (NSString *)fillModeForKey:(NSNumber *)key{
    switch ([key integerValue]) {
        case FillModeForwards:
            return kCAFillModeForwards;
        
        case FillModeBackwards:
            return kCAFillModeBackwards;
            
        case FillModeBoth:
            return kCAFillModeBoth;
            
        case FillModeRemoved:
            return kCAFillModeRemoved;
            
        default:
            return kCAFillModeRemoved;
    }
}

+ (id)valueForVariable:(NSString *)name fromDictionary:(NSDictionary *)dict necessarily:(BOOL)necessarily{
    if (!dict || !name)
        return nil;
    
    id value = dict[name];
    if (necessarily)
        NSAssert(value != nil, @"Value %@ is necessarily.", name);
    return value;
}

+ (NSString *)valuesTypeForAnimationKeyPathKey:(NSString *)animationKeyPathKey{
    if ([animationKeyPathKey isEqualToString:@"transform"])
        return kTransformValuesTypeKey;
    if ([animationKeyPathKey isEqualToString:@"opacity"])
        return kOpacityValuesTypeKey;
    return nil;
}

+ (CATransform3D)transformFromDictionary:(NSDictionary *)dictionary extraVariables:(NSDictionary *)eDictionary transformKey:(NSString *)key forLayer:(CALayer *)layer checkM34FromDictionary:(NSDictionary *)m34Dict{
    NSAssert(dictionary != nil, @"Transformation parameters is necessarily.");
    CATransform3D transform = layer.transform;
    
    BOOL shouldM34 = [[KAAnimationParser valueForVariable:kAnimationShouldM34Key fromDictionary:m34Dict necessarily:NO] boolValue];
    if (shouldM34) {
        double m34Low = [[KAAnimationParser valueForVariable:kAnimationM34Key fromDictionary:m34Dict necessarily:YES] doubleValue];
        transform.m34 = 1.0f/m34Low;
    }
    
    NSNumber *x = [KAAnimationParser valueForVariable:@"x" fromDictionary:dictionary necessarily:YES];
    NSNumber *y = [KAAnimationParser valueForVariable:@"y" fromDictionary:dictionary necessarily:YES];
    NSNumber *z = [KAAnimationParser valueForVariable:@"z" fromDictionary:dictionary necessarily:YES];
    
    NSString *eX = [KAAnimationParser valueForVariable:@"extra_x" fromDictionary:dictionary necessarily:NO];
    NSString *eY = [KAAnimationParser valueForVariable:@"extra_y" fromDictionary:dictionary necessarily:NO];
    NSString *eZ = [KAAnimationParser valueForVariable:@"extra_z" fromDictionary:dictionary necessarily:NO];
    
    if ([[eDictionary allKeys] containsObject:eX])
        x = eDictionary[eX];
    
    if ([[eDictionary allKeys] containsObject:eY])
        y = eDictionary[eY];
    
    if ([[eDictionary allKeys] containsObject:eZ])
        z = eDictionary[eZ];
    
    if ([key isEqualToString:kTransformValueTypeRotationKey]) {
        NSNumber *angle = [KAAnimationParser valueForVariable:@"angle" fromDictionary:dictionary necessarily:YES];
        NSString *eAngle = [KAAnimationParser valueForVariable:@"extra_angle" fromDictionary:dictionary necessarily:NO];
        
        if ([[eDictionary allKeys] containsObject:eAngle])
            angle = eDictionary[eAngle];
        
        transform = CATransform3DRotate(transform, DEGREES_TO_RADIANS([angle floatValue]), [x floatValue], [y floatValue], [z floatValue]);
    } else if ([key isEqualToString:kTransformValueTypeScaleKey]) {
        transform = CATransform3DScale(transform, [x floatValue], [y floatValue], [z floatValue]);
    } else if ([key isEqualToString:kTransformValueTypeTranslationKey]) {
        transform = CATransform3DTranslate(transform, [x floatValue], [y floatValue], [z floatValue]);
    }
    
    return transform;
}

+ (NSArray *)extraVariablesFromDictionary:(NSDictionary *)dictionary transformKey:(NSString *)key{
    NSMutableArray *mA = [@[] mutableCopy];
    
    NSString *extraXName = [KAAnimationParser valueForVariable:@"extra_x" fromDictionary:dictionary necessarily:NO];
    NSString *extraYName = [KAAnimationParser valueForVariable:@"extra_y" fromDictionary:dictionary necessarily:NO];
    NSString *extraZName = [KAAnimationParser valueForVariable:@"extra_z" fromDictionary:dictionary necessarily:NO];
    NSString *extraAngleName = nil;
    NSString *extraOpacity = [KAAnimationParser valueForVariable:@"extra_opacity" fromDictionary:dictionary necessarily:NO];
    
    if ([key isEqualToString:kTransformValueTypeRotationKey])
        extraAngleName = [KAAnimationParser valueForVariable:@"extra_angle" fromDictionary:dictionary necessarily:NO];
    
    if (extraXName)
        [mA addObject:extraXName];
    
    if (extraYName)
        [mA addObject:extraYName];
    
    if (extraZName)
        [mA addObject:extraZName];
    
    if (extraAngleName)
        [mA addObject:extraAngleName];
    
    if (extraOpacity)
        [mA addObject:extraOpacity];
    
    return [NSArray arrayWithArray:mA];
}

+ (NSArray *)extraVariablesNamesForAnimationAtPath:(NSString *)path{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSAssert([fm fileExistsAtPath:path], @"Animation file should exist.");
    
    NSData* plistData = [NSData dataWithContentsOfFile:path];
    NSError *error;
    NSPropertyListFormat format;
    NSDictionary *plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&format error:&error];
    NSAssert(error == nil, @"Can't read plist. Error: %@", [error localizedDescription]);
    
    NSString *animationKeyPath = [KAAnimationParser valueForVariable:kAnimationKeyPathKey fromDictionary:plist necessarily:YES];
    NSString *currentValuesType = [KAAnimationParser valuesTypeForAnimationKeyPathKey:animationKeyPath];
    
    NSDictionary *keyTimes = [KAAnimationParser valueForVariable:kAnimationKeyTimesKey fromDictionary:plist necessarily:YES];
    
    NSArray *sortedKeys = [[keyTimes allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSNumber *n1 = (NSNumber *)obj1;
        NSNumber *n2 = (NSNumber *)obj2;
        return [n1 compare:n2];
    }];
    
    NSMutableArray *extraVariables = [@[] mutableCopy];
    
    for (NSNumber *keyNumber in sortedKeys) {
        NSNumber *kNumber = @([keyNumber doubleValue]);
        id preValue = [KAAnimationParser valueForVariable:[NSString stringWithFormat:@"%@", kNumber] fromDictionary:keyTimes necessarily:NO];
        if ([animationKeyPath isEqualToString:@"transform"]) {
            if (preValue && [preValue isKindOfClass:[NSDictionary class]]) {
                NSDictionary *preValues = (NSDictionary *)preValue;
                if ([currentValuesType isEqualToString:kTransformValuesTypeKey]) {
                    NSDictionary *tValues = preValues[kTransformValuesTypeKey];
                    for (NSString *key in [tValues allKeys]) {
                        NSArray *eV = [KAAnimationParser extraVariablesFromDictionary:tValues[key] transformKey:key];
                        [extraVariables addObjectsFromArray:eV];
                    }
                }
            }
        } else if ([animationKeyPath isEqualToString:@"opacity"]) {
            if (preValue && [preValue isKindOfClass:[NSDictionary class]]) {
                id value = preValue[kOpacityValuesTypeKey];
                if ([value isKindOfClass:[NSDictionary class]]) {
                    NSArray *eV = [KAAnimationParser extraVariablesFromDictionary:value transformKey:nil];
                    [extraVariables addObjectsFromArray:eV];
                }
            }
        }
    }
    
    return [NSArray arrayWithArray:extraVariables];
}

+ (BOOL)shouldPreviewAnimationAtPath:(NSString *)path{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSAssert([fm fileExistsAtPath:path], @"Animation file should exist.");
    
    NSData* plistData = [NSData dataWithContentsOfFile:path];
    NSError *error;
    NSPropertyListFormat format;
    NSDictionary *plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&format error:&error];
    NSAssert(error == nil, @"Can't read plist. Error: %@", [error localizedDescription]);
    
    NSNumber *preview = [KAAnimationParser valueForVariable:kAnimationShouldPreviewKey fromDictionary:plist necessarily:YES];
    return [preview boolValue];
}

+ (void)setShouldPreview:(BOOL)should forAnimationAtPath:(NSString *)path{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSAssert([fm fileExistsAtPath:path], @"Animation file should exist.");
    
    NSData* plistData = [NSData dataWithContentsOfFile:path];
    NSError *error;
    NSPropertyListFormat format;
    NSDictionary *plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&format error:&error];
    NSAssert(error == nil, @"Can't read plist. Error: %@", [error localizedDescription]);
    
    [plist setValue:@(should) forKey:kAnimationShouldPreviewKey];
    [plist writeToFile:path atomically:YES];
}

@end
