//
//  KAAnimationParser.h
//  AnimationMaker
//
//  Created by Anton on 06.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAAnimation.h"

@interface KAAnimationParser : NSObject

+ (KAAnimation *)animationWithFileAtPath:(NSString *)path withExtraVariables:(NSDictionary *)eVars forLayer:(CALayer *)layer;
+ (NSArray *)extraVariablesNamesForAnimationAtPath:(NSString *)path;
+ (BOOL)shouldPreviewAnimationAtPath:(NSString *)path;
+ (void)setShouldPreview:(BOOL)should forAnimationAtPath:(NSString *)path;

@end
