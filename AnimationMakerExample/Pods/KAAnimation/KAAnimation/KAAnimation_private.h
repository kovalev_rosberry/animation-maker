//
//  KAAnimation_private.h
//  AnimationMaker
//
//  Created by Anton on 09.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#ifndef AnimationMaker_KAAnimation_private_h
#define AnimationMaker_KAAnimation_private_h

#if TARGET_IPHONE_SIMULATOR

#elif TARGET_OS_IPHONE

#elif TARGET_OS_MAC
#import <Quartz/Quartz.h>
#endif

@interface KAAnimation ()

@property (nonatomic, strong, readonly) CALayer *layer;
@property (nonatomic, strong, readonly) NSArray *extraVariables;

- (instancetype)initWithCAKeyFrameAnimation:(CAKeyframeAnimation *)animation fromFileWithPath:(NSString *)path forLayer:(CALayer *)layer withDelay:(CGFloat)delay;

@end

#endif
