//
//  CAAnimation.h
//  AnimationMaker
//
//  Created by Anton on 06.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

typedef void(^StartBlock)();
typedef void(^CompleteBlock)();

@interface KAAnimation : NSObject

- (void)playAnimation;
- (void)stopAnimation;

- (void)setWillStartBlock:(void(^)())startBlock;
- (void)setDidStartBlock:(void(^)())startBlock;
- (void)setCompleteBlock:(void(^)())completeBlock;

@property (nonatomic, assign) CGFloat customDuration;
@property (nonatomic, assign) CGFloat customDelay;
@property (nonatomic, strong, readonly) CALayer *layer;

@end
