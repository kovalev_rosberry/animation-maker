//
//  CAAnimation.m
//  AnimationMaker
//
//  Created by Anton on 06.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "KAAnimation.h"
#import "KAAnimation_private.h"

@interface KAAnimation ()

@property (nonatomic, strong) CAKeyframeAnimation *animation;
@property (nonatomic, strong) NSString *animationFilePath;

@property (nonatomic, copy) StartBlock willStartBlock;
@property (nonatomic, copy) StartBlock didStartBlock;
@property (nonatomic, copy) CompleteBlock completeBlock;

@property (nonatomic, strong) NSMutableDictionary *eVariables;

@property (nonatomic, assign) CGFloat delay;

@end

@implementation KAAnimation

- (instancetype)initWithCAKeyFrameAnimation:(CAKeyframeAnimation *)animation fromFileWithPath:(NSString *)path forLayer:(CALayer *)layer withDelay:(CGFloat)delay
{
    self = [super init];
    if (self) {
        [self setAnimation:animation];
        [self.animation setDelegate:self];
        [self setAnimationFilePath:path];
        _eVariables = [@{} mutableCopy];
        _layer = layer;
        _delay = delay;
    }
    return self;
}

- (void)playAnimation{
    if (_extraVariables.count != _eVariables.count)
        NSLog(@"WARNING! YOU DIDN'T SET UP ALL EXTRA VARIABLES");
    else
    {
        
    }
    if (self.willStartBlock)
        self.willStartBlock();
    CGFloat delay = self.customDelay ?: self.delay;
    [self.animation setBeginTime:CACurrentMediaTime() + delay];
    if (self.customDuration)
        [self.animation setDuration:self.customDuration];
    [_layer addAnimation:self.animation forKey:[NSString stringWithFormat:@"%@", (NSString *)self]];
}

- (void)playAnimationForLayer:(CALayer *)layer{
    _layer = layer;
    [self playAnimation];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (self.completeBlock)
        self.completeBlock();
}

- (void)animationDidStart:(CAAnimation *)anim{
    if (self.didStartBlock)
        self.didStartBlock();
}

- (void)stopAnimation{
    [self.layer removeAllAnimations];
}

- (void)setValue:(id)value forExtraVariable:(NSString *)variable{
    NSAssert([_extraVariables containsObject:variable], @"There are no extra variable with name %@", variable);
    [_eVariables setObject:value forKey:variable];
    [self.animation setDelegate:self];
}

- (void)setWillStartBlock:(void (^)())startBlock{
    _willStartBlock = startBlock;
}

- (void)setDidStartBlock:(void (^)())startBlock{
    _didStartBlock = startBlock;
}

- (void)setCompleteBlock:(void(^)())completeBlock{
    _completeBlock = completeBlock;
}

- (void)dealloc
{
    _willStartBlock = nil;
    _didStartBlock = nil;
    _completeBlock = nil;
}

@end
