//
//  KAAnimationParserStrings.h
//  AnimationMaker
//
//  Created by Anton on 09.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#ifndef AnimationMaker_KAAnimationParserStrings_h
#define AnimationMaker_KAAnimationParserStrings_h

static NSString *kAnimationKeyPathKey = @"KAAnimationKeyPathKey";
static NSString *kAnimationTimingFunctionsKey = @"KATimingFunctionsKey";
static NSString *kTransformValuesTypeKey = @"KATransformValuesTypeKey";
static NSString *kTransformValueTypeRotationKey = @"KATransformValueTypeRotationKey";
static NSString *kTransformValueTypeTranslationKey = @"KATransformValueTypeTranslationKey";
static NSString *kTransformValueTypeScaleKey = @"KATransformValueTypeScaleKey";
static NSString *kOpacityValuesTypeKey = @"KAOpacityValuesTypeKey";
static NSString *kAnimationKeyTimesKey = @"KAAnimationKeyTimesKey";
static NSString *kAnimationDurationKey = @"KAAnimationDurationKey";
static NSString *kAnimationDelayKey = @"KAAnimationDelayKey";
static NSString *kAnimationFillModeKey = @"KAAnimationFillModeKey";
static NSString *kAnimationAutoReverseKey = @"KAAnimationAutoReverseKey";
static NSString *kAnimationRemoveOnCompletionKey = @"KAAnimationRemoveOnCompletionKey";
static NSString *kAnimationRepeatKey = @"KAAnimationRepeatKey";
static NSString *kAnimationRepeatCountKey = @"KAAnimationRepeatCountKey";
static NSString *kAnimationRepeatForeverKey = @"KAAnimationRepeatForeverKey";
static NSString *kAnimationRepeatDurationKey = @"KAAnimationRepeatDurationKey";
static NSString *kAnimationShouldPreviewKey = @"KAAnimationShouldPreviewKey";
static NSString *kAnimationShouldM34Key = @"KAAnimationShouldM34Key";
static NSString *kAnimationM34Key = @"KAAnimationM34Key";

#endif
