//
//  CreateOrEditAnimationWindowController.m
//  AnimationMaker
//
//  Created by Anton on 08.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#define kTransformationType             @"Transformation"
#define kOpacityType                    @"Opacity (Alpha)"
#define kRotateTransformationType       @"Rotation"
#define kTranslateTransformationType    @"Translation"
#define kScaleTransformationType        @"Scale"
#define kMinimumTimeLinePercent         0.05f

#define VariableName(variable)          (@""#variable)

#import "CreateOrEditAnimationWindowController.h"
#import "NumbersOnlyFormatter.h"
#import "KAAnimationParserStrings.h"
#import "AdditionalOptionsWindowController.h"

@interface CreateOrEditAnimationWindowController () <NSTextFieldDelegate>

@property (strong) IBOutlet NSTextField *animationNameLabel;
@property (strong) IBOutlet NSPopUpButton *animationTypePopupButton;
@property (strong) IBOutlet NSTextField *animationDurationTextField;
@property (strong) IBOutlet NSStepper *animationDurationStepper;
@property (strong) IBOutlet NSTextField *animationDelayTextField;
@property (strong) IBOutlet NSStepper *animationDelayStepper;
@property (strong) IBOutlet NSPopUpButton *transformationTypePopupButton;
@property (strong) IBOutlet NSTextField *xAxisTextField;
@property (strong) IBOutlet NSStepper *xAxisStepper;
@property (strong) IBOutlet NSTextField *yAxisTextField;
@property (strong) IBOutlet NSStepper *yAxisStepper;
@property (strong) IBOutlet NSTextField *zAxisTextField;
@property (strong) IBOutlet NSStepper *zAxisStepper;
@property (strong) IBOutlet NSTextField *angleTextField;
@property (strong) IBOutlet NSStepper *angleStepper;
@property (strong) IBOutlet NSTextField *opacityAlphaTextField;
@property (strong) IBOutlet NSStepper *opacityAlphaStepper;
@property (strong) IBOutlet NSButton *editInitialObjectState;
@property (strong) IBOutlet NSButton *setUpExtraVariablesButton;
@property (strong) IBOutlet NSView *simpleVariablesView;
@property (strong) IBOutlet NSTextField *xExtraLabel;
@property (strong) IBOutlet NSTextField *yExtraLabel;
@property (strong) IBOutlet NSTextField *zExtraLabel;
@property (strong) IBOutlet NSTextField *angleExtraLabel;
@property (strong) IBOutlet NSTextField *opacityExtraLabel;

@property (strong) IBOutlet NSView *extraVariablesView;
@property (strong) IBOutlet NSButton *extraDoneButton;
@property (strong) IBOutlet NSButton *extraXCheckBox;
@property (strong) IBOutlet NSTextField *extraXTextField;
@property (strong) IBOutlet NSButton *extraYCheckBox;
@property (strong) IBOutlet NSTextField *extraYTextField;
@property (strong) IBOutlet NSButton *extraZCheckBox;
@property (strong) IBOutlet NSTextField *extraZTextField;
@property (strong) IBOutlet NSButton *extraAngleCheckBox;
@property (strong) IBOutlet NSTextField *extraAngleTextField;
@property (strong) IBOutlet NSButton *extraOpacityCheckBox;
@property (strong) IBOutlet NSTextField *extraOpacityTextField;

@property (strong) IBOutlet NSSegmentedControl *timeLineSegmentedControl;
@property (strong) IBOutlet NSButton *addSegmentBeforeButton;
@property (strong) IBOutlet NSButton *addSegmentAfterButton;
@property (strong) IBOutlet NSButton *deleteSegmentButton;
@property (strong) IBOutlet NSTextField *segmentDurationTextField;
@property (strong) IBOutlet NSStepper *segmentDurationStepper;
@property (strong) IBOutlet NSMatrix *radioButtonMatrix;
@property (strong) IBOutlet NSMatrix *segmentTimingFunctionRadioMatrix;

@property (strong) IBOutlet NSButton *cancelButton;
@property (strong) IBOutlet NSButton *doneButton;

@property (nonatomic, assign) CGFloat animationDurationValue;
@property (nonatomic, assign) CGFloat animationDelayValue;
@property (nonatomic, assign) CGFloat opacityAlphaValue;
@property (nonatomic, assign) CGFloat xAxisValue;
@property (nonatomic, assign) CGFloat yAxisValue;
@property (nonatomic, assign) CGFloat zAxisValue;
@property (nonatomic, assign) CGFloat angleValue;
@property (nonatomic, assign) CGFloat segmentDurationValue;

@property (nonatomic, strong) NSMutableDictionary *animationDictionary;

@property (nonatomic, strong) NSString *currentTransformationTypeKey;

@end

@implementation CreateOrEditAnimationWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    NumbersOnlyFormatter *formatter = [[NumbersOnlyFormatter alloc] init];
    [formatter setDecimalSeparator:@"."];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setAllowsFloats:YES];
    [formatter setMinimumFractionDigits:0];
    [formatter setMaximumFractionDigits:4];
    [formatter setPartialStringValidationEnabled:YES];
    [self.animationDurationTextField setFormatter:formatter];
    [self.animationDelayTextField setFormatter:formatter];
    [self.opacityAlphaTextField setFormatter:formatter];
    [self.xAxisTextField setFormatter:formatter];
    [self.yAxisTextField setFormatter:formatter];
    [self.zAxisTextField setFormatter:formatter];
    [self.angleTextField setFormatter:formatter];
    [self.segmentDurationTextField setFormatter:formatter];
    
    for (int i = 0; i < self.segmentTimingFunctionRadioMatrix.cells.count; ++i)
        [self.segmentTimingFunctionRadioMatrix setToolTip:[self tooltipForCellAtIndex:i] forCell:[self.segmentTimingFunctionRadioMatrix cellAtRow:i column:0]];
    
    NSString *eToolTip = @"This variable setted as EXTRA variable. It means that it should be dynamic and you will set it later in code.\n\nYou still able to preview animation with value you entered usual way.";
    [self.xExtraLabel setToolTip:eToolTip];
    [self.yExtraLabel setToolTip:eToolTip];
    [self.zExtraLabel setToolTip:eToolTip];
    [self.angleExtraLabel setToolTip:eToolTip];
    [self.opacityExtraLabel setToolTip:eToolTip];
    
    [self changeAnimationTypeToTransformation:YES];
    [self changeTransformationTypeToRotate:NO];
    
    if (_animationDictionary) {
        if ([_animationDictionary[kAnimationKeyPathKey] isEqualToString:@"opacity"]) {
            [self.animationTypePopupButton selectItemAtIndex:1];
            [self changeAnimationTypeToTransformation:NO];
        }
        
        [self.animationDurationTextField setStringValue:[NSString stringWithFormat:@"%@", (_animationDictionary[kAnimationDurationKey]) ?: @"0"]];
        [self.animationDelayTextField setStringValue:[NSString stringWithFormat:@"%@", (_animationDictionary[kAnimationDelayKey]) ?: @"0"]];
//        NSString *str = [_animationDictionary objectForKey:kAnimationKeyPathKey];
//        if (!str)
//            [_animationDictionary setObject:@"transform" forKey:kAnimationKeyPathKey];
        [self reFillTextFields];
        [self setTimeLineSegmentDuration];
    }
    
    [self stepperValueChanged:self.animationDelayStepper];
    [self stepperValueChanged:self.animationDurationStepper];
    
    [self redrawTimeLine];
    
    [self.timeLineSegmentedControl setTarget:self];
    [self.timeLineSegmentedControl setAction:@selector(timeLineSegmentChanged:)];
    
    NSString *name = [[[self.pathToPlist componentsSeparatedByString:@"/"] lastObject] stringByDeletingPathExtension];
    [self.animationNameLabel setStringValue:name];
}

- (NSString *)tooltipForCellAtIndex:(NSInteger)idx{
    switch (idx) {
        case 0:
            return @"Specifies linear pacing. Linear pacing causes an animation to occur evenly over its duration.";
            
        case 1:
            return @"Specifies ease-in pacing. Ease-in pacing causes the animation to begin slowly, and then speed up as it progresses.";
            
        case 2:
            return @"Specifies ease-out pacing. An ease-out pacing causes the animation to begin quickly, and then slow as it completes.";
            
        case 3:
            return @"Specifies ease-in ease-out pacing. An ease-in ease-out animation begins slowly, accelerates through the middle of its duration, and then slows again before completing.";
            
        case 4:
            return @"Specifies the timing function used as the default by most animations. It approximates a Bézier timing function using the control points [(0.0,0.0), (0.25,0.1), (0.25,0.1), (1.0,1.0)]. By using this function you ensure that your animations will use the current default timing.";
            
        default:
            return @"";
    }
}

- (void)setNilValueForKey:(NSString*)key
{
    if ([key isEqualToString:VariableName(animationDurationValue)])
    {
        self.animationDurationValue = 0;
        return;
    }
    if ([key isEqualToString:VariableName(animationDelayValue)])
    {
        self.animationDelayValue = 0;
        return;
    }
    if ([key isEqualToString:VariableName(opacityAlphaValue)])
    {
        self.opacityAlphaValue = 0;
        return;
    }
    if ([key isEqualToString:VariableName(xAxisValue)])
    {
        self.xAxisValue = 0;
        return;
    }
    if ([key isEqualToString:VariableName(yAxisValue)])
    {
        self.yAxisValue = 0;
        return;
    }
    if ([key isEqualToString:VariableName(zAxisValue)])
    {
        self.zAxisValue = 0;
        return;
    }
    if ([key isEqualToString:VariableName(angleValue)])
    {
        self.angleValue = 0;
        return;
    }
    if ([key isEqualToString:VariableName(segmentDurationValue)])
    {
        self.segmentDurationValue = 0;
        return;
    }
    
    [super setNilValueForKey:key];
}

- (void)setPathToPlist:(NSString *)pathToPlist{
    _pathToPlist = pathToPlist;
    _animationDictionary = [[NSDictionary dictionaryWithContentsOfFile:pathToPlist] mutableCopy];
    if (![[_animationDictionary objectForKey:@"FirstSetupComplete"] boolValue]) {
        [_animationDictionary setObject:@NO forKey:kAnimationRepeatKey];
        [_animationDictionary setObject:@0 forKey:kAnimationRepeatCountKey];
        [_animationDictionary setObject:@0 forKey:kAnimationDurationKey];
        [_animationDictionary setObject:@NO forKey:kAnimationAutoReverseKey];
        [_animationDictionary setObject:@3 forKey:kAnimationFillModeKey];
        [_animationDictionary setObject:@NO forKey:kAnimationRepeatForeverKey];
        [_animationDictionary setObject:@YES forKey:kAnimationRemoveOnCompletionKey];
        [_animationDictionary setObject:@"transform" forKey:kAnimationKeyPathKey];
        [_animationDictionary setObject:@YES forKey:@"FirstSetupComplete"];
    }
    if (!_animationDictionary)
        _animationDictionary = [@{} mutableCopy];
}

#pragma mark - Methods

- (void)timeLineSegmentChanged:(NSSegmentedControl *)segmentedControl{
    [self checkTimeLineSelection];
    [self reFillTextFields];
    [self setTimeLineSegmentDuration];
}

- (void)setTimeLineSegmentDuration{
    NSMutableArray *sortedKeys = [self sortedKeyTimesKeys];
    double currentSegmentStartTime = [sortedKeys[self.timeLineSegmentedControl.selectedSegment] doubleValue];
    double currentSegmentEndTime = [sortedKeys[self.timeLineSegmentedControl.selectedSegment+1] doubleValue];
    double currentSegmentDuration = currentSegmentEndTime - currentSegmentStartTime;
    [self.segmentDurationTextField setStringValue:[NSString stringWithFormat:@"%@", @(currentSegmentDuration)]];
    [self stepperValueChanged:self.segmentDurationStepper];
    
    NSNumber *tf = [[[self keyTimes] objectForKey:sortedKeys[self.timeLineSegmentedControl.selectedSegment+1]] objectForKey:kAnimationTimingFunctionsKey];
    if ([tf integerValue] < self.segmentTimingFunctionRadioMatrix.cells.count)
        [self.segmentTimingFunctionRadioMatrix selectCellAtRow:[tf integerValue] column:0];
}

- (NSString *)currentTransformationTypeKey{
    if ([self.transformationTypePopupButton.selectedItem.title isEqualToString:kRotateTransformationType]) {
        return kTransformValueTypeRotationKey;
    } else if ([self.transformationTypePopupButton.selectedItem.title isEqualToString:kTranslateTransformationType]) {
        return kTransformValueTypeTranslationKey;
    } else if ([self.transformationTypePopupButton.selectedItem.title isEqualToString:kScaleTransformationType]) {
        return  kTransformValueTypeScaleKey;
    }
    return nil;
}

- (void)changeAnimationTypeToTransformation:(BOOL)transform{
    [self.opacityAlphaTextField setEnabled:!transform];
    [self.opacityAlphaStepper setEnabled:!transform];
    [self.xAxisTextField setEnabled:transform];
    [self.xAxisStepper setEnabled:transform];
    [self.yAxisTextField setEnabled:transform];
    [self.yAxisStepper setEnabled:transform];
    [self.zAxisTextField setEnabled:transform];
    [self.zAxisStepper setEnabled:transform];
    if ([self.transformationTypePopupButton.selectedItem.title isEqualToString:kRotateTransformationType]) {
        [self.angleTextField setEnabled:transform];
        [self.angleStepper setEnabled:transform];
    }
    [self.transformationTypePopupButton setEnabled:transform];
}

- (void)changeTransformationTypeToRotate:(BOOL)rotate{
    [self.angleTextField setEnabled:rotate];
    [self.angleStepper setEnabled:rotate];
}

- (IBAction)animationTypeChanged:(NSPopUpButton *)sender {
    NSString *aType;
    if ([sender.selectedItem.title isEqualToString:kTransformationType]) {
        [self changeAnimationTypeToTransformation:YES];
        aType = @"transform";
    } else if ([sender.selectedItem.title isEqualToString:kOpacityType]) {
        [self changeAnimationTypeToTransformation:NO];
        aType = @"opacity";
    }
    [_animationDictionary setObject:aType forKey:kAnimationKeyPathKey];
}

- (void)setAnimationProperty:(id)property forKey:(NSString *)key{
    NSMutableDictionary *keyTimes = [self keyTimes];
    NSMutableArray *sortedKeys = [self sortedKeyTimesKeys];
    NSUInteger index = (self.editInitialObjectState.state == NSOnState) ? 0 : self.timeLineSegmentedControl.selectedSegment + 1;
    
    NSMutableDictionary *parDict = keyTimes[sortedKeys[index]];
    if (!parDict || [parDict isEqual:[NSNull null]])
        parDict = [@{} mutableCopy];
    [parDict setObject:property forKey:key];
    
    [keyTimes setObject:parDict forKey:sortedKeys[index]];
    [_animationDictionary setObject:keyTimes forKey:kAnimationKeyTimesKey];
}

- (id)animationPropertyForKey:(NSString *)key{
    NSMutableDictionary *keyTimes = _animationDictionary[kAnimationKeyTimesKey];
    if (!keyTimes)
        return nil;
    NSMutableArray *sortedKeys = [self sortedKeyTimesKeys];
    NSUInteger index = (self.editInitialObjectState.state == NSOnState) ? 0 : self.timeLineSegmentedControl.selectedSegment + 1;
    
    NSMutableDictionary *parDict = keyTimes[sortedKeys[index]];
    if (!parDict || [parDict isEqual:[NSNull null]] || ![parDict isKindOfClass:[NSDictionary class]])
        return nil;
    return [parDict objectForKey:key];
}

- (IBAction)transformationTypeChanged:(NSPopUpButton *)sender {
    if ([sender.selectedItem.title isEqualToString:kRotateTransformationType]) {
        [self changeTransformationTypeToRotate:YES];
    } else {
        [self changeTransformationTypeToRotate:NO];
    }
    [self reFillTextFields];
}

- (void)reFillTextFields{
    NSDictionary *transformDict = [self animationPropertyForKey:kTransformValuesTypeKey];
    if (transformDict) {
        if ([self.currentTransformationTypeKey isEqualToString:kTransformValueTypeTranslationKey]) {
            NSDictionary *translateDict = transformDict[kTransformValueTypeTranslationKey];
            [self.xAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (translateDict[@"x"]) ?: @"0"]];
            [self.yAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (translateDict[@"y"]) ?: @"0"]];
            [self.zAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (translateDict[@"z"]) ?: @"0"]];

            NSString *extraX = translateDict[@"extra_x"];
            NSString *extraY = translateDict[@"extra_y"];
            NSString *extraZ = translateDict[@"extra_z"];
            [self refillExtraFieldsWithX:extraX y:extraY z:extraZ angle:nil extraOpacity:nil];
            
        } else if ([self.currentTransformationTypeKey isEqualToString:kTransformValueTypeRotationKey]) {
            NSDictionary *rotateDict = transformDict[kTransformValueTypeRotationKey];
            [self.xAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (rotateDict[@"x"]) ?: @"0"]];
            [self.yAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (rotateDict[@"y"]) ?: @"0"]];
            [self.zAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (rotateDict[@"z"]) ?: @"0"]];
            [self.angleTextField setStringValue:[NSString stringWithFormat:@"%@", (rotateDict[@"angle"]) ?: @"0"]];
            [self stepperValueChanged:self.angleStepper];
            
            NSString *extraX = rotateDict[@"extra_x"];
            NSString *extraY = rotateDict[@"extra_y"];
            NSString *extraZ = rotateDict[@"extra_z"];
            NSString *extraAngle = rotateDict[@"extra_angle"];
            [self refillExtraFieldsWithX:extraX y:extraY z:extraZ angle:extraAngle extraOpacity:nil];
            
        } else if ([self.currentTransformationTypeKey isEqualToString:kTransformValueTypeScaleKey]) {
            NSDictionary *scaleDict = transformDict[kTransformValueTypeScaleKey];
            [self.xAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (scaleDict[@"x"]) ?: @"1"]];
            [self.yAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (scaleDict[@"y"]) ?: @"1"]];
            [self.zAxisTextField setStringValue:[NSString stringWithFormat:@"%@", (scaleDict[@"z"]) ?: @"1"]];
            
            NSString *extraX = scaleDict[@"extra_x"];
            NSString *extraY = scaleDict[@"extra_y"];
            NSString *extraZ = scaleDict[@"extra_z"];
            [self refillExtraFieldsWithX:extraX y:extraY z:extraZ angle:nil extraOpacity:nil];
        }
    } else {
        if ([self.currentTransformationTypeKey isEqualToString:kTransformValueTypeTranslationKey]) {
            [self.xAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"0"]];
            [self.yAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"0"]];
            [self.zAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"0"]];
        } else if ([self.currentTransformationTypeKey isEqualToString:kTransformValueTypeRotationKey]) {
            [self.xAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"0"]];
            [self.yAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"0"]];
            [self.zAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"0"]];
            [self.angleTextField setStringValue:[NSString stringWithFormat:@"%@", @"0"]];
            [self stepperValueChanged:self.angleStepper];
        } else if ([self.currentTransformationTypeKey isEqualToString:kTransformValueTypeScaleKey]) {
            [self.xAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"1"]];
            [self.yAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"1"]];
            [self.zAxisTextField setStringValue:[NSString stringWithFormat:@"%@", @"1"]];
        }
    }
//    else {
//        NSNumber *opacityValue = [self animationPropertyForKey:kOpacityValuesTypeKey];
//        if (!opacityValue)
//            opacityValue = @(1);
//        [self.opacityAlphaTextField setStringValue:[NSString stringWithFormat:@"%@", opacityValue]];
//        [self stepperValueChanged:self.opacityAlphaStepper];
//    }
    [self stepperValueChanged:self.xAxisStepper];
    [self stepperValueChanged:self.yAxisStepper];
    [self stepperValueChanged:self.zAxisStepper];
    
    NSDictionary *opacityDict = [self animationPropertyForKey:kOpacityValuesTypeKey];
    NSNumber *opacityValue = opacityDict[@"opacity"];
    if (!opacityValue)
        opacityValue = @(1);
    [self.opacityAlphaTextField setStringValue:[NSString stringWithFormat:@"%@", opacityValue]];
    if ([_animationDictionary[kAnimationKeyPathKey] isEqualToString:@"opacity"]) {
        NSString *extraOpacity = opacityDict[@"extra_opacity"];
        [self refillExtraFieldsWithX:nil y:nil z:nil angle:nil extraOpacity:extraOpacity];
    }
    [self stepperValueChanged:self.opacityAlphaStepper];
}

- (IBAction)stepperValueChanged:(NSStepper *)sender {
    NSTextField *tf;
    if ([sender isEqual:self.animationDelayStepper]) {
        tf = self.animationDelayTextField;
    }
    if ([sender isEqual:self.animationDurationStepper]) {
        tf = self.animationDurationTextField;
    }
    if ([sender isEqual:self.xAxisStepper]) {
        tf = self.xAxisTextField;
    }
    if ([sender isEqual:self.yAxisStepper]) {
        tf = self.yAxisTextField;
    }
    if ([sender isEqual:self.zAxisStepper]) {
        tf = self.zAxisTextField;
    }
    if ([sender isEqual:self.angleStepper]) {
        tf = self.angleTextField;
    }
    if ([sender isEqual:self.opacityAlphaStepper]) {
        tf = self.opacityAlphaTextField;
    }
    if ([sender isEqual:self.segmentDurationStepper]) {
        tf = self.segmentDurationTextField;
    }
    NSNotification *n = [NSNotification notificationWithName:@"CustomNote" object:tf];
    [self controlTextDidChange:n];
}

- (IBAction)editInitialObjectStateCheckboxChecked:(id)sender {
    [self reFillTextFields];
}

- (IBAction)doneAction:(NSButton *)sender {
    NSDictionary *dToSave = [_animationDictionary dictionaryByReplacingNullsWithStrings];
    if ([dToSave writeToFile:self.pathToPlist atomically:YES])
        [NSApp endSheet:self.window returnCode:NSModalResponseOK];
}

- (IBAction)cancelAction:(NSButton *)sender {
    [NSApp endSheet:self.window returnCode:NSModalResponseCancel];
}

//- (BOOL) validateMyValue:(inout NSString **)newValue error:(out NSError **)outError {
//	BOOL isEntirelyNumeric;
//	//Determine whether the whole string (perhaps after stripping whitespace) is a number. If not, reject it outright.
//	if (isEntirelyNumeric) {
//		//The input was @"12", or it was @" 12 " or something and you stripped the whitespace from it, so *newValue is @"12".
//		return YES;
//	} else {
//		if (outError) {
//			*outError = [NSError errorWithDomain:NSCocoaErrorDomain code: NSKeyValueValidationError userInfo:nil];
//		}
//		//Note: No need to set *newValue here.
//        NSBeep();
//		return NO;
//	}
//}

- (void)controlTextDidChange:(NSNotification *)notification{
    if ([notification.object isEqual:self.animationDurationTextField]) {
        double value = [self.animationDurationTextField.stringValue doubleValue];
        [_animationDictionary setObject:@(value) forKey:kAnimationDurationKey];
        if (self.animationDurationValue != value)
            self.animationDurationValue = value;
    } else if ([notification.object isEqual:self.animationDelayTextField]) {
        double value = [self.animationDelayTextField.stringValue doubleValue];
        [_animationDictionary setObject:@(value) forKey:kAnimationDelayKey];
        if (self.animationDelayValue != value)
            self.animationDelayValue = value;
    } else if ([notification.object isEqual:self.opacityAlphaTextField]) {
        double value = [self.opacityAlphaTextField.stringValue doubleValue];
//        [self setAnimationProperty:@(value) forKey:kOpacityValuesTypeKey];
        [self setOpacityValue:@(value) forKey:@"opacity"];
        if (self.opacityAlphaValue != value)
            self.opacityAlphaValue = value;
    } else if ([notification.object isEqual:self.xAxisTextField]) {
        double value = [self.xAxisTextField.stringValue doubleValue];
        [self setTransformationValue:@(value) forKey:@"x"];
        if (self.xAxisValue != value)
            self.xAxisValue = value;
    } else if ([notification.object isEqual:self.yAxisTextField]) {
        double value = [self.yAxisTextField.stringValue doubleValue];
        [self setTransformationValue:@(value) forKey:@"y"];
        if (self.yAxisValue != value)
            self.yAxisValue = value;
    } else if ([notification.object isEqual:self.zAxisTextField]) {
        double value = [self.zAxisTextField.stringValue doubleValue];
        [self setTransformationValue:@(value) forKey:@"z"];
        if (self.zAxisValue != value)
            self.zAxisValue = value;
    } else if ([notification.object isEqual:self.angleTextField]) {
        double value = [self.angleTextField.stringValue doubleValue];
        [self setTransformationValue:@(value) forKey:@"angle"];
        if (self.angleValue != value)
            self.angleValue = value;
    } else if ([notification.object isEqual:self.segmentDurationTextField]) {
        double value = [self.segmentDurationTextField.stringValue doubleValue];
        if (value < .05f)
            value = .05f;
        
        if (value < .1f) {
            [self.addSegmentAfterButton setEnabled:NO];
            [self.addSegmentBeforeButton setEnabled:NO];
        } else {
            [self.addSegmentAfterButton setEnabled:YES];
            [self.addSegmentBeforeButton setEnabled:YES];
        }
        
        double max = [self maximumAvailableDurationForCurrentSegment];
        if (value > max)
            value = max;
        
        if (self.segmentDurationValue != value)
            self.segmentDurationValue = value;
        [self currentSegmentTimeDuratonChanged];
    } else if ([notification.object isEqual:self.extraXTextField]) {
        NSString *value = self.extraXTextField.stringValue;
        [self setTransformationValue:value forKey:@"extra_x"];
    } else if ([notification.object isEqual:self.extraYTextField]) {
        NSString *value = self.extraYTextField.stringValue;
        [self setTransformationValue:value forKey:@"extra_y"];
    } else if ([notification.object isEqual:self.extraZTextField]) {
        NSString *value = self.extraZTextField.stringValue;
        [self setTransformationValue:value forKey:@"extra_z"];
    } else if ([notification.object isEqual:self.extraAngleTextField]) {
        NSString *value = self.extraAngleTextField.stringValue;
        [self setTransformationValue:value forKey:@"extra_angle"];
    } else if ([notification.object isEqual:self.extraOpacityTextField]) {
        NSString *value = self.extraOpacityTextField.stringValue;
        [self setOpacityValue:value forKey:@"extra_opacity"];
    }
}

- (void)setTransformationValue:(id)value forKey:(NSString *)key{
    NSMutableDictionary *keyTimes = [self keyTimes];
    NSMutableArray *sortedKeys = [self sortedKeyTimesKeys];
    NSUInteger index = (self.editInitialObjectState.state == NSOnState) ? 0 : self.timeLineSegmentedControl.selectedSegment + 1;
    
    NSMutableDictionary *parDict = keyTimes[sortedKeys[index]];
    if (!parDict || [parDict isEqual:[NSNull null]] || ![parDict isKindOfClass:[NSDictionary class]])
        parDict = [@{} mutableCopy];
    NSMutableDictionary *dict = parDict[kTransformValuesTypeKey];
    if (!dict)
        dict = [@{} mutableCopy];
    NSMutableDictionary *d = dict[self.currentTransformationTypeKey];
    if (!d)
        d = [@{} mutableCopy];
    if (value)
        [d setValue:value forKey:key];
    else
        [d removeObjectForKey:key];
    [dict setObject:d forKey:self.currentTransformationTypeKey];
    [parDict setValue:dict forKey:kTransformValuesTypeKey];
    
    [keyTimes setObject:parDict forKey:sortedKeys[index]];
    [_animationDictionary setObject:keyTimes forKey:kAnimationKeyTimesKey];
}

- (void)setOpacityValue:(id)value forKey:(NSString *)key{
    NSMutableDictionary *keyTimes = [self keyTimes];
    NSMutableArray *sortedKeys = [self sortedKeyTimesKeys];
    NSUInteger index = (self.editInitialObjectState.state == NSOnState) ? 0 : self.timeLineSegmentedControl.selectedSegment + 1;
    
    NSMutableDictionary *parDict = keyTimes[sortedKeys[index]];
    if (!parDict || [parDict isEqual:[NSNull null]] || ![parDict isKindOfClass:[NSDictionary class]])
        parDict = [@{} mutableCopy];
    NSMutableDictionary *dict = parDict[kOpacityValuesTypeKey];
    if (!dict)
        dict = [@{} mutableCopy];
    if (value)
        [dict setValue:value forKey:key];
    else
        [dict removeObjectForKey:key];
    [parDict setValue:dict forKey:kOpacityValuesTypeKey];
    
    [keyTimes setObject:parDict forKey:sortedKeys[index]];
    [_animationDictionary setObject:keyTimes forKey:kAnimationKeyTimesKey];
}

- (NSMutableArray *)sortedKeyTimesKeys{
    NSMutableDictionary *keyTimes = [self keyTimes];
    NSMutableArray *sortedKeys = [[[keyTimes allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSNumber *n1;
        NSNumber *n2;
        if ([obj1 isKindOfClass:[NSString class]])
            n1 = @([obj1 doubleValue]);
        else
            n1 = (NSNumber *)obj1;
        
        if ([obj2 isKindOfClass:[NSString class]])
            n2 = @([obj2 doubleValue]);
        else
            n2 = (NSNumber *)obj2;
        
        return [n1 compare:n2];
    }] mutableCopy];
    return sortedKeys;
}

- (NSMutableDictionary *)keyTimes{
    NSMutableDictionary *keyTimes = _animationDictionary[kAnimationKeyTimesKey];
    if (!keyTimes) {
        keyTimes = [@{@"0":[NSNull null], @"1":[NSNull null]} mutableCopy];
        [_animationDictionary setObject:keyTimes forKey:kAnimationKeyTimesKey];
    }
    return keyTimes;
}

- (void)checkTimeLineSelection{
    NSMutableDictionary *keyTimes = [self keyTimes];
    if (keyTimes.count <= 2) {
        [self.segmentDurationTextField setEnabled:NO];
        [self.segmentDurationStepper setEnabled:NO];
        [self.radioButtonMatrix setEnabled:NO];
    } else {
        [self.segmentDurationTextField setEnabled:YES];
        [self.segmentDurationStepper setEnabled:YES];
        [self.radioButtonMatrix setEnabled:YES];
        if (self.timeLineSegmentedControl.selectedSegment == 0) {
            [self.radioButtonMatrix selectCellAtRow:1 column:0];
            [self.radioButtonMatrix setEnabled:NO];
        } else if (self.timeLineSegmentedControl.selectedSegment == keyTimes.count - 2) {
            [self.radioButtonMatrix selectCellAtRow:0 column:0];
            [self.radioButtonMatrix setEnabled:NO];
        }
    }
}

- (double)maximumAvailableDurationForCurrentSegment{
    NSArray *sortedKeyTimes = [self sortedKeyTimesKeys];
    
    NSInteger prevIndex = self.timeLineSegmentedControl.selectedSegment;
    NSInteger curIndex = self.timeLineSegmentedControl.selectedSegment + 1;
    NSInteger nextIndex = self.timeLineSegmentedControl.selectedSegment + 2;
    
    BOOL isNextSegmentAvailable = (nextIndex < sortedKeyTimes.count);
    BOOL isPrevSegmentAvailable = (prevIndex > 0);
    double prevSegmentStartTime = (isPrevSegmentAvailable) ? [sortedKeyTimes[prevIndex - 1] doubleValue] : 0;
    double nextSegmentEndTime = (isNextSegmentAvailable) ? [sortedKeyTimes[nextIndex] doubleValue] : 1;
    
    if ([self.radioButtonMatrix selectedRow] == 0) {
        double nStartTime = f_round(prevSegmentStartTime, 2);
        if (isPrevSegmentAvailable)
            nStartTime = f_round(prevSegmentStartTime + .05, 2);
        double currentSegmentEndTime = [sortedKeyTimes[curIndex] doubleValue];
        return f_round(currentSegmentEndTime - nStartTime, 2);
    } else {
        double nEndTime = f_round(nextSegmentEndTime - .05, 2);
        double currentSegmentStartTime = [sortedKeyTimes[prevIndex] doubleValue];
        return f_round(nEndTime - currentSegmentStartTime, 2);
    }
    
    return 0;
}

- (IBAction)timingFunctionChanged:(id)sender{
    NSArray *sortedKeyTimes = [self sortedKeyTimesKeys];
    id key = sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment+1];
    NSMutableDictionary *keyTimes = [self keyTimes];
    [keyTimes[key] setObject:@(self.segmentTimingFunctionRadioMatrix.selectedRow) forKey:kAnimationTimingFunctionsKey];
}

- (IBAction)setUpExtraVariables:(NSButton *)sender {
    [self.extraVariablesView setHidden:NO];
    [self.simpleVariablesView setHidden:YES];
    [self reFillTextFields];
}

- (IBAction)extraDonePressed:(NSButton *)sender {
    [self.extraVariablesView setHidden:YES];
    [self.simpleVariablesView setHidden:NO];
    [self reFillTextFields];
}

- (IBAction)extraCheckBoxClicked:(NSButton *)sender {
    NSTextField *tf;
    NSString *key;
    if ([sender isEqualTo:self.extraXCheckBox]) {
        tf = self.extraXTextField;
        key = @"extra_x";
    } else if ([sender isEqualTo:self.extraYCheckBox]) {
        tf = self.extraYTextField;
        key = @"extra_y";
    } else if ([sender isEqualTo:self.extraZCheckBox]) {
        tf = self.extraZTextField;
        key = @"extra_z";
    } else if ([sender isEqualTo:self.extraAngleCheckBox]) {
        tf = self.extraAngleTextField;
        key = @"extra_angle";
    } else if ([sender isEqualTo:self.extraOpacityCheckBox]) {
        tf = self.extraOpacityTextField;
        key = @"extra_opacity";
    }
    if (sender.state == NSOnState) {
        [self setTransformationValue:tf.stringValue forKey:key];
    } else if (sender.state == NSOffState) {
        if ([_animationDictionary[kAnimationKeyPathKey] isEqualToString:@"opacity"])
            [self setOpacityValue:nil forKey:key];
        else
            [self setTransformationValue:nil forKey:key];
    }
    [tf setEnabled:(sender.state == NSOnState) ? YES : NO];
}

- (void)refillExtraFieldsWithX:(NSString *)extraX y:(NSString *)extraY z:(NSString *)extraZ angle:(NSString *)extraAngle extraOpacity:(NSString *)extraOpacity{
    if ([extraX isEqualToString:@""])
        extraX = nil;
    if ([extraY isEqualToString:@""])
        extraY = nil;
    if ([extraZ isEqualToString:@""])
        extraZ = nil;
    if ([extraAngle isEqualToString:@""])
        extraAngle = nil;
    if ([extraOpacity isEqualToString:@""])
        extraOpacity = nil;
    
    [self.extraXTextField setStringValue:[NSString stringWithFormat:@"%@", (extraX) ?: @""]];
    [self.extraYTextField setStringValue:[NSString stringWithFormat:@"%@", (extraY) ?: @""]];
    [self.extraZTextField setStringValue:[NSString stringWithFormat:@"%@", (extraZ) ?: @""]];
    [self.extraAngleTextField setStringValue:[NSString stringWithFormat:@"%@", (extraAngle) ?: @""]];
    [self.extraOpacityTextField setStringValue:[NSString stringWithFormat:@"%@", (extraOpacity) ?: @""]];
    
    BOOL animationTypeOpacity = [_animationDictionary[kAnimationKeyPathKey] isEqualToString:@"opacity"];
    
    [self.opacityAlphaTextField setEnabled:(extraOpacity || !animationTypeOpacity) ? NO : YES];
    [self.opacityAlphaStepper setEnabled:self.opacityAlphaTextField.isEnabled];
    [self.opacityExtraLabel setHidden:(extraOpacity) ? NO : YES];
    [self.extraOpacityCheckBox setState:(extraOpacity && animationTypeOpacity) ? NSOnState : NSOffState];
    [self.extraOpacityCheckBox setEnabled:animationTypeOpacity];
    [self.extraOpacityTextField setEnabled:(self.extraOpacityCheckBox.state == NSOnState)];
    
    [self.xAxisTextField setEnabled:(extraX || animationTypeOpacity) ? NO : YES];
    [self.xAxisStepper setEnabled:self.xAxisTextField.isEnabled];
    [self.xExtraLabel setHidden:(extraX) ? NO : YES];
    [self.extraXCheckBox setState:(extraX) ? NSOnState : NSOffState];
    [self.extraXCheckBox setEnabled:!animationTypeOpacity];
    [self.extraXTextField setEnabled:(self.extraXCheckBox.state == NSOnState) && self.extraXCheckBox.isEnabled];
    
    [self.yAxisTextField setEnabled:(extraY || animationTypeOpacity) ? NO : YES];
    [self.yAxisStepper setEnabled:self.yAxisTextField.isEnabled];
    [self.yExtraLabel setHidden:(extraY) ? NO : YES];
    [self.extraYCheckBox setState:(extraY) ? NSOnState : NSOffState];
    [self.extraYCheckBox setEnabled:!animationTypeOpacity];
    [self.extraYTextField setEnabled:(self.extraYCheckBox.state == NSOnState) && self.extraYCheckBox.isEnabled];
    
    [self.zAxisTextField setEnabled:(extraZ || animationTypeOpacity) ? NO : YES];
    [self.zAxisStepper setEnabled:self.zAxisTextField.isEnabled];
    [self.zExtraLabel setHidden:(extraZ) ? NO : YES];
    [self.extraZCheckBox setState:(extraZ) ? NSOnState : NSOffState];
    [self.extraZCheckBox setEnabled:!animationTypeOpacity];
    [self.extraZTextField setEnabled:(self.extraZCheckBox.state == NSOnState) && self.extraZCheckBox.isEnabled];
    
    [self.angleStepper setEnabled:self.angleTextField.isEnabled];
    [self.angleExtraLabel setHidden:(extraAngle) ? NO : YES];
    [self.extraAngleTextField setEnabled:(extraAngle) ? YES : NO];
    [self.extraAngleCheckBox setState:(extraAngle && [self.transformationTypePopupButton.selectedItem.title isEqualToString:kRotateTransformationType]) ? NSOnState : NSOffState];
    [self.extraAngleCheckBox setEnabled:!animationTypeOpacity && [self.transformationTypePopupButton.selectedItem.title isEqualToString:kRotateTransformationType]];
    [self.extraAngleTextField setEnabled:(self.extraAngleCheckBox.state == NSOnState) && self.extraAngleCheckBox.isEnabled];
}

- (IBAction)advancedOptionsClicked:(NSButton *)sender {
    __block AdditionalOptionsWindowController *wc = [[AdditionalOptionsWindowController alloc] initWithWindowNibName:@"AdditionalOptionsWindowController" andAnimationDictionary:[NSDictionary dictionaryWithDictionary:_animationDictionary]];
    [NSApp beginSheet:wc.window modalForWindow:self.window didEndBlock:^(NSInteger returnCode) {
        _animationDictionary = wc.animationDictionary;
        wc = nil;
    }];
}

#pragma mark - Segments

- (IBAction)addSegmentBefore:(NSButton *)sender {
    NSArray *sortedKeyTimes = [self sortedKeyTimesKeys];
    double currentSegmentStartTime = [sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment] doubleValue];
    double currentSegmentEndTime = [sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment+1] doubleValue];
    double currentSegmentDuration = currentSegmentEndTime - currentSegmentStartTime;
    
    if (currentSegmentDuration < .1f)
        return;
    
    double nSegmentStartTime = currentSegmentStartTime + currentSegmentDuration/2.f;
    
    id key = sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment+1];
    
    NSMutableDictionary *keyTimes = [self keyTimes];
    [keyTimes setObject:@"" forKey:[NSString stringWithFormat:@"%@", @(nSegmentStartTime)]];
    [keyTimes setObject:keyTimes[key] forKey:key];
    
    [self redrawTimeLine];
    [self.timeLineSegmentedControl setSelectedSegment:self.timeLineSegmentedControl.selectedSegment+1];
    [self setTimeLineSegmentDuration];
    [self checkTimeLineSelection];
}

- (IBAction)addSegmentAfter:(NSButton *)sender {
    NSArray *sortedKeyTimes = [self sortedKeyTimesKeys];
    double currentSegmentStartTime = [sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment] doubleValue];
    double currentSegmentEndTime = [sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment+1] doubleValue];
    double currentSegmentDuration = currentSegmentEndTime - currentSegmentStartTime;
    
    if (currentSegmentDuration < .1f)
        return;
    
    double nSegmentStartTime = currentSegmentStartTime + currentSegmentDuration/2.f;
    
    id key = sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment+1];
    
    NSMutableDictionary *keyTimes = [self keyTimes];
    [keyTimes setObject:keyTimes[key] forKey:[NSString stringWithFormat:@"%@", @(nSegmentStartTime)]];
    [keyTimes setObject:@"" forKey:key];
    
    [self redrawTimeLine];
    [self setTimeLineSegmentDuration];
    [self checkTimeLineSelection];
}

- (IBAction)deleteSelectedSegment:(NSButton *)sender {
    NSArray *sortedKeyTimes = [self sortedKeyTimesKeys];
    if (sortedKeyTimes.count <= 2)
        return;
    
    [NSAlert showSheetModalForWindow:self.window
                             message:@"This action will remove this timeline segment. It can't be undone."
                     informativeText:@"Proceed anyway?"
                          alertStyle:NSCriticalAlertStyle
                   cancelButtonTitle:@"Cancel"
                   otherButtonTitles:@[@"OK"]
                           onDismiss:^(NSUInteger buttonIndex) {
                               if (buttonIndex == 0) {
                                   id key = sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment+1];
                                   NSMutableDictionary *keyTimes = [self keyTimes];
                                   [keyTimes removeObjectForKey:key];
                                   [self redrawTimeLine];
                                   [self setTimeLineSegmentDuration];
                                   [self checkTimeLineSelection];
                               }
                           }onCancel:^{
                               
                           }];
}

- (void)redrawTimeLine{
    CGRect timeLineFrame = self.timeLineSegmentedControl.frame;
    timeLineFrame.size.width -= 6;
    NSArray *sortedKeyTimes = [self sortedKeyTimesKeys];
    
    [self.timeLineSegmentedControl setSegmentCount:sortedKeyTimes.count-1];
    
    NSNumber *prevNumber = nil;
    NSUInteger k = -1;
    for (NSNumber *number in sortedKeyTimes) {
        if (prevNumber) {
            double num = [number doubleValue];
            double prevNum = [prevNumber doubleValue];
            
            double duration = num - prevNum;
            
            [self.timeLineSegmentedControl setWidth:duration*timeLineFrame.size.width forSegment:k];
            [self.timeLineSegmentedControl setLabel:[NSString stringWithFormat:@"%@ - %@", @(prevNum), @(num)] forSegment:k];
        }
        prevNumber = number;
        ++k;
    }
    [self checkTimeLineSelection];
}

- (void)currentSegmentTimeDuratonChanged{
    NSArray *sortedKeyTimes = [self sortedKeyTimesKeys];
    NSMutableDictionary *keyTimes = [self keyTimes];

    double currentSegmentStartTime = [sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment] doubleValue];
    double currentSegmentEndTime = [sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment+1] doubleValue];
    double currentSegmentDuration = currentSegmentEndTime - currentSegmentStartTime;
    
    id curKey = sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment+1];
    NSDictionary *currentSegmentDictionaty = keyTimes[curKey];
    id prevKey = sortedKeyTimes[self.timeLineSegmentedControl.selectedSegment];
    NSDictionary *prevSegmentDictionary = keyTimes[prevKey];
    
    float dif = f_round(currentSegmentDuration - self.segmentDurationValue, 2);
    float absDif = ABS(dif);
    BOOL reduce = (dif > 0) ? YES : NO;
    
    if (fequal(dif, 0))
        return;
    
    id removeKey;
    NSDictionary *nObject;
    double nKey;
    
    //
    //Left side
    //
    if ([self.radioButtonMatrix selectedRow] == 0) {
        removeKey = prevKey;
        nObject = prevSegmentDictionary;
        if (reduce)
            nKey = f_round(currentSegmentStartTime + absDif, 2);
        else
            nKey = f_round(currentSegmentStartTime - absDif, 2);
    } else {
        //
        //Right side
        //
        if (reduce)
            nKey = f_round(currentSegmentEndTime - absDif, 2);
        else
            nKey = f_round(currentSegmentEndTime + absDif, 2);
        removeKey = curKey;
        nObject = currentSegmentDictionaty;
    }
    
    [keyTimes removeObjectForKey:removeKey];
    [keyTimes setObject:nObject forKey:[NSString stringWithFormat:@"%@", @(nKey)]];
    
    [self redrawTimeLine];
}

double f_round(double dval, int n)
{
    char l_fmtp[32], l_buf[64];
    char *p_str;
    sprintf (l_fmtp, "%%.%df", n);
    if (dval>=0)
        sprintf (l_buf, l_fmtp, dval);
    else
        sprintf (l_buf, l_fmtp, dval);
    return ((double)strtod(l_buf, &p_str));
}

@end
