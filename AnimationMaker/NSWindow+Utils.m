//
//  NSWindow+Utils.m
//  AnimationMaker
//
//  Created by Anton on 09.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "NSWindow+Utils.h"
#import <objc/runtime.h>

@implementation NSWindow (Utils)

@dynamic modalSheetEndBlock;

- (ModalSheetEndBlock)modalSheetEndBlock{
    return objc_getAssociatedObject(self, @selector(modalSheetEndBlock));
}

- (void)setModalSheetEndBlock:(ModalSheetEndBlock)modalSheetEndBlock{
    objc_setAssociatedObject(self, @selector(modalSheetEndBlock), modalSheetEndBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
