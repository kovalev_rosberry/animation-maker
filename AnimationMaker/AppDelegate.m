//
//  AppDelegate.m
//  AnimationMaker
//
//  Created by Anton on 30.04.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "AppDelegate.h"
#import "MasterViewController.h"

@interface  AppDelegate()

@property (nonatomic,strong) IBOutlet MasterViewController *masterViewController;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    self.masterViewController = [[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil];
    
    [self.window.contentView addSubview:self.masterViewController.view];
    self.masterViewController.view.frame = ((NSView*)self.window.contentView).bounds;
    
    [self.window setReleasedWhenClosed:NO];
    
//    [[NSUserDefaults standardUserDefaults] setObject:@(1000)
//                                              forKey:@"NSInitialToolTipDelay"];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag{
    [self.window setIsVisible:YES];
    return YES;
}

@end
