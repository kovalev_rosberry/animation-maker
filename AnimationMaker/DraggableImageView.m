//
//  DraggableImageView.m
//  AnimationMaker
//
//  Created by Anton on 30.04.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "DraggableImageView.h"

@interface DraggableImageView (){
    CGPoint lastDragLocation;
}

@end

@implementation DraggableImageView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

- (void)mouseDown:(NSEvent *)theEvent{
    lastDragLocation = [theEvent locationInWindow];
}

- (void)mouseDragged:(NSEvent *)theEvent{
    if (!self.canBeMoved)
        return;
    NSPoint newDragLocation = [theEvent locationInWindow];
    NSPoint origin = [self frame].origin;
    origin.x += (-lastDragLocation.x + newDragLocation.x);
    origin.y -= (-lastDragLocation.y + newDragLocation.y);
    [self setFrameOrigin:origin];
    lastDragLocation = newDragLocation;
//    NSLog(@"%@", NSStringFromPoint(origin));
}

//- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
//{
//    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
//        UIView *piece = self.moveView;//gestureRecognizer.view;
//        CGPoint locationInView = [gestureRecognizer locationInView:piece];
//        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
//        
//        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
//        piece.center = locationInSuperview;
//    }
//}

@end
