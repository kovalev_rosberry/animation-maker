//
//  NameProcessWindowController.m
//  AnimationMaker
//
//  Created by Anton on 07.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "NameProcessWindowController.h"

@interface NameProcessWindowController ()

@property (strong) IBOutlet NSButton *okButton;
@property (strong) IBOutlet NSButton *cancelButton;

@end

@implementation NameProcessWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)awakeFromNib{
    [self.okButton setTarget:self];
    [self.okButton setAction:@selector(okButtonPressed)];
    [self.cancelButton setTarget:self];
    [self.cancelButton setAction:@selector(cancelButtonPressed)];
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void)okButtonPressed{
    if ([[self.nameTextField.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
        return;
    [NSApp endSheet:self.window returnCode:NSModalResponseOK];
}

- (void)cancelButtonPressed{
    [NSApp endSheet:self.window returnCode:NSModalResponseCancel];
}

@end
