//
//  CustomCellView.h
//  AnimationMaker
//
//  Created by Anton on 18.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CustomCellView : NSTableCellView

@property (nonatomic, strong) IBOutlet NSButton *checkBoxButton;

@end
