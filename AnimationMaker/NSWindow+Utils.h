//
//  NSWindow+Utils.h
//  AnimationMaker
//
//  Created by Anton on 09.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef void(^ModalSheetEndBlock)(NSInteger returnCode);

@interface NSWindow (Utils)

@property (nonatomic, copy) ModalSheetEndBlock modalSheetEndBlock;

@end
