//
//  AppDelegate.h
//  AnimationMaker
//
//  Created by Anton on 30.04.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
