//
//  DraggableImageView.h
//  AnimationMaker
//
//  Created by Anton on 30.04.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DraggableImageView : NSImageView

@property (nonatomic, assign) BOOL canBeMoved;
@property (nonatomic, strong) NSString *name;

@end
