//
//  NameProcessWindowController.h
//  AnimationMaker
//
//  Created by Anton on 07.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NameProcessWindowController : NSWindowController

@property (strong) IBOutlet NSTextField *nameTextField;

@end
