//
//  MasterViewController.m
//  AnimationMaker
//
//  Created by Anton on 30.04.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "MasterViewController.h"
#import <Quartz/Quartz.h>
#import "DraggableImageView.h"
#import "NameProcessWindowController.h"
#import "CreateOrEditAnimationWindowController.h"
#import "KAAnimationParser.h"
#import "CustomCellView.h"
#import "KAAnimationParserStrings.h"

#define kBasicTableViewDragAndDropDataType @"BasicTableViewDragAndDropDataType"

@interface MasterViewController () <NSTableViewDataSource, NSTableViewDelegate>

@property (strong) IBOutlet NSView *iPhoneView;
@property (strong) IBOutlet NSButton *addObjectButton;
@property (strong) IBOutlet NSButton *animateButton;
@property (strong) IBOutlet NSTableView *imagesTableView;
@property (strong) IBOutlet NSTableView *animationsTableView;
@property (strong) IBOutlet NSButton *addImageButton;
@property (strong) IBOutlet NSButton *removeImageButton;
@property (strong) IBOutlet NSButton *addAnimationButton;
@property (strong) IBOutlet NSButton *removeAnimationButton;
@property (strong) IBOutlet NSButton *exportAnimationButton;
@property (strong) IBOutlet NSButton *importAnimationButton;

@property (strong, nonatomic) DraggableImageView *currentAnimatedIV;
@property (strong, nonatomic) NSArray *imagesNames;
@property (strong, nonatomic) NSArray *animations;
@property (strong, nonatomic) NSArray *previewAnimations;
@property (assign, nonatomic) BOOL notFirstAppear;

@property (strong, nonatomic) CreateOrEditAnimationWindowController *createOrEditWC;

@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)awakeFromNib{
    [self.iPhoneView setWantsLayer:YES];
    [self.iPhoneView.layer setBackgroundColor:[NSColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f].CGColor];
    
    [self.addObjectButton setTarget:self];
    [self.addObjectButton setAction:@selector(addObjectToAnimate:)];
    
    [self.animateButton setTarget:self];
    [self.animateButton setAction:@selector(animateObject)];
    
    if (!self.notFirstAppear) {
        [self reFillImagesArray];
        self.notFirstAppear = YES;
    }
    
    [self.animationsTableView setTarget:self];
    [self.animationsTableView setDoubleAction:@selector(doubleClicked:)];
    
    [self.imagesTableView setTarget:self];
    [self.imagesTableView setDoubleAction:@selector(doubleClicked:)];
}

- (void)reFillImagesArray{
    NSString *path = [[NSBundle mainBundle] bundlePath];
    path = [path stringByAppendingPathComponent:@"Images"];
    NSFileManager *fm = [NSFileManager defaultManager];
    
    BOOL isDirectory = NO;
    BOOL exist = [fm fileExistsAtPath:path isDirectory:&isDirectory];
    if (!exist || !isDirectory)
        [fm createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSArray *fileList = [fm subpathsAtPath:path];
    NSMutableArray *directoryList = [[NSMutableArray alloc] init];
    for (NSString *file in fileList) {
        NSString *subPath = [path stringByAppendingPathComponent:file];
        BOOL isDir = NO;
        [fm fileExistsAtPath:subPath isDirectory:(&isDir)];
        if(isDir)
            [directoryList addObject:file];
    }
    
    self.imagesNames = directoryList;
    [self.imagesTableView reloadData];
}

- (void)reFillAnimationsArray{
    NSMutableArray *ma = [@[] mutableCopy];
    NSString *path = [[NSBundle mainBundle] bundlePath];
    path = [path stringByAppendingPathComponent:@"Images"];
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSUInteger index = self.imagesTableView.selectedRow;
    
    if (index != -1 && index < self.imagesNames.count) {
        path = [path stringByAppendingPathComponent:self.imagesNames[index]];
        
        NSArray *fileList = [fm subpathsAtPath:path];
        NSMutableArray *directoryList = [[NSMutableArray alloc] init];
        for (NSString *file in fileList) {
            NSString *subPath = [path stringByAppendingPathComponent:file];
            BOOL isDir = NO;
            [fm fileExistsAtPath:subPath isDirectory:(&isDir)];
            if(!isDir && [[subPath pathExtension] isEqualToString:@"plist"]) {
                NSString *fName = [file stringByDeletingPathExtension];
                [directoryList addObject:fName];
                if ([KAAnimationParser shouldPreviewAnimationAtPath:subPath])
                    [ma addObject:fName];
            }
        }
        self.animations = [NSArray arrayWithArray:directoryList];
        self.previewAnimations = [NSArray arrayWithArray:ma];
    } else {
        self.animations = @[];
        self.previewAnimations = @[];
    }
    
    [self.animationsTableView reloadData];
}

- (void)addObjectToAnimate:(NSButton *)b{
    
}

- (IBAction)addImage:(id)sender {
    NSOpenPanel *panel = [NSOpenPanel openPanel];
    [panel setCanChooseFiles:YES];
    [panel setCanChooseDirectories:NO];
    [panel setAllowsMultipleSelection:NO];
    [panel setMessage:@"Select image file"];
    [panel setAllowedFileTypes:[NSImage imageTypes]];
    [panel beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            for (NSURL *fileURL in [panel URLs]) {
                NameProcessWindowController *wc = [[NameProcessWindowController alloc] initWithWindowNibName:@"NameProcessWindowController"];
                [NSApp beginSheet:wc.window modalForWindow:self.view.window didEndBlock:^(NSInteger returnCode) {
                    if (returnCode == NSModalResponseOK) {
                        NSString *filePath = [[NSBundle mainBundle] bundlePath];
                        filePath = [filePath stringByAppendingPathComponent:@"Images"];
                        NSFileManager *fm = [NSFileManager defaultManager];
                        
                        BOOL isDirectory = NO;
                        BOOL exist = [fm fileExistsAtPath:filePath isDirectory:&isDirectory];
                        NSError *error;
                        if (!exist || !isDirectory) {
                            [fm createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error];
                            [self logError:&error];
                        }
                        
                        filePath = [filePath stringByAppendingPathComponent:wc.nameTextField.stringValue];
                        
                        isDirectory = NO;
                        exist = [fm fileExistsAtPath:filePath isDirectory:&isDirectory];
                        if (exist && isDirectory) {
                            NSDateFormatter *f = [[NSDateFormatter alloc] init];
                            [f setDateFormat:@"d.MM.yyyy HH:mm:ss"];
                            filePath = [[filePath stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@ (%@)", wc.nameTextField.stringValue, [f stringFromDate:[NSDate date]]]];
                            [fm createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error];
                            [self logError:&error];
                        } else if (!exist) {
                            [fm createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error];
                            [self logError:&error];
                        }
                        
                        filePath = [filePath stringByAppendingPathComponent:@"mainImage"];
                        filePath = [filePath stringByAppendingPathExtension:[fileURL pathExtension]];
                        
                        [fm copyItemAtPath:[fileURL path] toPath:filePath error:&error];
                        [self logError:&error];
                        
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:filePath]];
                        NSImage *image = [[NSImage alloc] initWithData:imageData];
                        DraggableImageView *iv = [[DraggableImageView alloc] init];
                        [iv setName:wc.nameTextField.stringValue];
                        [iv setImage:image];
                        [iv setFrame:NSRectFromCGRect(CGRectMake(0, 0, image.size.width, image.size.height))];
                        [iv setCanBeMoved:YES];
                        [iv setWantsLayer:YES];
//                        [iv.layer setBackgroundColor:[NSColor colorWithRed:1.f green:0.f blue:0.f alpha:1.f].CGColor];
                        [self.iPhoneView addSubview:iv];
                        
                        [self reFillImagesArray];
                    }
                }];
            }
        }
    }];
}

- (IBAction)removeImage:(id)sender {
    [NSAlert showSheetModalForWindow:self.view.window
                             message:@"This action will remove all animations you create for this object. It can't be undone."
                     informativeText:@"Proceed anyway?"
                          alertStyle:NSCriticalAlertStyle
                   cancelButtonTitle:@"Cancel"
                   otherButtonTitles:@[@"OK"]
                           onDismiss:^(NSUInteger buttonIndex) {
                               if (buttonIndex == 0) {
                                   NSString *path = [[NSBundle mainBundle] bundlePath];
                                   path = [path stringByAppendingPathComponent:@"Images"];
                                   NSFileManager *fm = [NSFileManager defaultManager];
                                   
                                   path = [path stringByAppendingPathComponent:self.imagesNames[self.imagesTableView.selectedRow]];
                                   NSError *error;
                                   [fm removeItemAtPath:path error:&error];
                                   [self logError:&error];
                                   [self reFillAnimationsArray];
                                   [self reFillImagesArray];
                               }
                           }onCancel:^{
                               
                           }];
}

- (IBAction)addAnimation:(id)sender {
    NameProcessWindowController *wc = [[NameProcessWindowController alloc] initWithWindowNibName:@"NameProcessWindowController"];
    [NSApp beginSheet:wc.window modalForWindow:self.view.window didEndBlock:^(NSInteger returnCode) {
        if (returnCode == NSModalResponseOK) {
            NSString *filePath = [[NSBundle mainBundle] bundlePath];
            filePath = [filePath stringByAppendingPathComponent:@"Images"];
            NSFileManager *fm = [NSFileManager defaultManager];
            
            BOOL isDirectory = NO;
            BOOL exist = [fm fileExistsAtPath:filePath isDirectory:&isDirectory];
            NSError *error;
            if (!exist || !isDirectory) {
                [fm createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error];
                [self logError:&error];
            }
            
            filePath = [filePath stringByAppendingPathComponent:self.imagesNames[self.imagesTableView.selectedRow]];
            filePath = [filePath stringByAppendingPathComponent:wc.nameTextField.stringValue];
            filePath = [filePath stringByAppendingPathExtension:@"plist"];
            
            exist = [fm fileExistsAtPath:filePath];
            if (exist) {
                NSDateFormatter *f = [[NSDateFormatter alloc] init];
                [f setDateFormat:@"d.MM.yyyy HH:mm:ss"];
                filePath = [[filePath stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@ (%@)", wc.nameTextField.stringValue, [f stringFromDate:[NSDate date]]]];
            }
            
            NSDictionary *d = @{kAnimationShouldPreviewKey:@YES};
            [d writeToFile:filePath atomically:YES];
            [self reFillAnimationsArray];
        }
    }];
}

- (IBAction)removeAnimation:(id)sender {
    [NSAlert showSheetModalForWindow:self.view.window
                             message:@"This action can't be undone."
                     informativeText:@"Proceed anyway?"
                          alertStyle:NSCriticalAlertStyle
                   cancelButtonTitle:@"Cancel"
                   otherButtonTitles:@[@"OK"]
                           onDismiss:^(NSUInteger buttonIndex) {
                               if (buttonIndex == 0) {
                                   NSString *path = [[NSBundle mainBundle] bundlePath];
                                   path = [path stringByAppendingPathComponent:@"Images"];
                                   NSFileManager *fm = [NSFileManager defaultManager];

                                   path = [path stringByAppendingPathComponent:self.imagesNames[self.imagesTableView.selectedRow]];
                                   path = [path stringByAppendingPathComponent:self.animations[self.animationsTableView.selectedRow]];
                                   path = [path stringByAppendingPathExtension:@"plist"];
                                   NSError *error;
                                   [fm removeItemAtPath:path error:&error];
                                   [self logError:&error];
                                   
                                   [self reFillAnimationsArray];
                                   [self reFillImagesArray];
                                   
                                   NSNotification *n = [NSNotification notificationWithName:@"CustomNote" object:self.imagesTableView];
                                   [self tableViewSelectionDidChange:n];
                                   n = [NSNotification notificationWithName:@"CustomNote" object:self.animationsTableView];
                                   [self tableViewSelectionDidChange:n];
                               }
                           }onCancel:^{
                               
                           }];
}

- (void)exportAnimation{
    NSSavePanel *panel = [NSSavePanel savePanel];
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    path = [path stringByAppendingPathComponent:@"Images"];
    NSUInteger index = self.imagesTableView.selectedRow;
    
    if (index != -1 && index < self.imagesNames.count) {
        path = [path stringByAppendingPathComponent:self.imagesNames[index]];
        index = self.animationsTableView.selectedRow;
        if (index != -1 && index < self.animations.count) {
            path = [path stringByAppendingPathComponent:self.animations[self.animationsTableView.selectedRow]];
            path = [path stringByAppendingPathExtension:@"plist"];
        }
    }
    [panel setNameFieldStringValue:[path lastPathComponent]];
    [panel beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result) {
        if (result == NSOKButton) {
            NSString *savePath = [[panel URL] path];
            NSError *error = nil;
            [[NSFileManager defaultManager] copyItemAtPath:path toPath:savePath error:&error];
            [self logError:&error];
        }
    }];
}

- (void)importAnimation{
    NSOpenPanel *panel = [NSOpenPanel openPanel];
    [panel setCanChooseFiles:YES];
    [panel setCanChooseDirectories:NO];
    [panel setAllowsMultipleSelection:NO];
    [panel setMessage:@"Select animation file"];
    [panel setAllowedFileTypes:@[@"plist"]];
    [panel beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            for (NSURL *fileURL in [panel URLs]) {
                NSString *path = [[NSBundle mainBundle] bundlePath];
                path = [path stringByAppendingPathComponent:@"Images"];
                NSUInteger index = self.imagesTableView.selectedRow;
                
                if (index != -1 && index < self.imagesNames.count) {
                    path = [path stringByAppendingPathComponent:self.imagesNames[index]];
                    path = [path stringByAppendingPathComponent:[[[fileURL path] componentsSeparatedByString:@"/"] lastObject]];
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                        [NSAlert showSheetModalForWindow:self.view.window
                                                 message:@"File with same name already exists."
                                         informativeText:@""
                                              alertStyle:NSInformationalAlertStyle
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil
                                               onDismiss:^(NSUInteger buttonIndex) {
                                                   
                                               } onCancel:^{
                                                   
                                               }];
                    } else
                        [[NSFileManager defaultManager] copyItemAtURL:fileURL toURL:[NSURL fileURLWithPath:path] error:nil];
                    [self reFillAnimationsArray];
                } else {
                    [NSAlert showSheetModalForWindow:self.view.window
                                             message:@"Seems like you didn't choose image -_-"
                                     informativeText:@""
                                          alertStyle:NSInformationalAlertStyle
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil
                                           onDismiss:^(NSUInteger buttonIndex) {
                                               
                                           } onCancel:^{
                                               
                                           }];
                }
            }
        }
    }];
}

- (void)logError:(NSError **)error{
    if (*error) {
        NSLog(@"%@", [*error localizedDescription]);
        *error = nil;
    }
}

- (void)animateObject{
    for (NSView *sv in self.iPhoneView.subviews) {
        if ([sv isKindOfClass:[DraggableImageView class]]) {
            DraggableImageView *iv = (DraggableImageView *)sv;
            [iv.layer removeAllAnimations];
            
            NSString *path = [[NSBundle mainBundle] bundlePath];
            path = [path stringByAppendingPathComponent:@"Images"];
            path = [path stringByAppendingPathComponent:iv.name];
            NSFileManager *fm = [NSFileManager defaultManager];
            
            NSArray *fileList = [fm subpathsAtPath:path];
            for (NSString *file in fileList) {
                if ([[file pathExtension] isEqualToString:@"plist"]) {
                    path = [path stringByAppendingPathComponent:file];
                    if ([KAAnimationParser shouldPreviewAnimationAtPath:path]) {
                        NSArray *eVars = [KAAnimationParser extraVariablesNamesForAnimationAtPath:path];
                        NSMutableDictionary *dict = [@{} mutableCopy];
                        for (NSString *eName in eVars) {
                            if ([eName isEqualToString:@"MoveX"]) {
                                [dict setObject:@(100) forKey:eName];
                            }
                        }
                        KAAnimation *animation = [KAAnimationParser animationWithFileAtPath:path withExtraVariables:[NSDictionary dictionaryWithDictionary:dict] forLayer:iv.layer];
                        [animation setWillStartBlock:^{
                            [iv setCanBeMoved:NO];
                        }];
                        [animation setCompleteBlock:^{
                            [iv setCanBeMoved:YES];
                        }];
                        [animation playAnimation];
                    }
                    path = [path stringByDeletingLastPathComponent];
                }
            }
        }
    }
}

-(void)setAnchorPoint:(CGPoint)anchorPoint forView:(NSView *)view
{
    CGPoint newPoint = CGPointMake(view.bounds.size.width * anchorPoint.x, view.bounds.size.height * anchorPoint.y);
    CGPoint oldPoint = CGPointMake(view.bounds.size.width * view.layer.anchorPoint.x, view.bounds.size.height * view.layer.anchorPoint.y);
    
    newPoint = CGPointApplyAffineTransform(newPoint, view.layer.affineTransform);
    oldPoint = CGPointApplyAffineTransform(oldPoint, view.layer.affineTransform);
    
    CGPoint position = view.layer.position;
    
    position.x -= oldPoint.x;
    position.x += newPoint.x;
    
    position.y -= oldPoint.y;
    position.y += newPoint.y;
    
    view.layer.position = position;
    view.layer.anchorPoint = anchorPoint;
}

- (void)doubleClicked:(NSTableView *)table{
    if ([table isEqual:self.animationsTableView]) {
        _createOrEditWC = [[CreateOrEditAnimationWindowController alloc] initWithWindowNibName:@"CreateOrEditAnimationWindowController"];
        NSInteger index = self.animationsTableView.selectedRow;
        if (index >= 0) {
            NSString *path = [self pathForAnimationWithName:[self.animations objectAtIndex:index]];
            if (path) {
                [_createOrEditWC setPathToPlist:path];
                [NSApp beginSheet:_createOrEditWC.window modalForWindow:self.view.window didEndBlock:^(NSInteger returnCode) {
                    _createOrEditWC = nil;
                }];
            }
        }
    } else {
        NSString *path = [[NSBundle mainBundle] bundlePath];
        path = [path stringByAppendingPathComponent:@"Images"];
        NSFileManager *fm = [NSFileManager defaultManager];
        
        NSUInteger index = self.imagesTableView.selectedRow;
        
        if (index != -1 && index < self.imagesNames.count) {
            path = [path stringByAppendingPathComponent:self.imagesNames[index]];
            
            NSArray *fileList = [fm subpathsAtPath:path];
            for (NSString *file in fileList) {
                NSString *fileName = [[file componentsSeparatedByString:@"/"] lastObject];
                fileName = [fileName stringByDeletingPathExtension];
                if ([fileName isEqualToString:@"mainImage"]) {
                    path = [path stringByAppendingPathComponent:file];
                }
            }
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:path]];
            NSImage *image = [[NSImage alloc] initWithData:imageData];
            DraggableImageView *iv = [[DraggableImageView alloc] init];
            [iv setName:self.imagesNames[index]];
            [iv setImage:image];
            [iv setFrame:NSRectFromCGRect(CGRectMake(0, 0, image.size.width, image.size.height))];
            [iv setCanBeMoved:YES];
            [iv setWantsLayer:YES];
//            [iv.layer setBackgroundColor:[NSColor colorWithRed:1.f green:0.f blue:0.f alpha:1.f].CGColor];
            [self.iPhoneView addSubview:iv];
        }
    }
}

- (IBAction)exportAnimation:(NSButton *)sender {
    [self exportAnimation];
}

- (IBAction)importAnimation:(id)sender {
    [self importAnimation];
}

#pragma mark - Table

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    if ([tableView isEqual:self.imagesTableView]) {
        NSTableCellView *cellView = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
        [cellView.textField setStringValue:self.imagesNames[row]];
        return cellView;
    } else if ([tableView isEqual:self.animationsTableView]) {
        CustomCellView *cellView = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
        [cellView.textField setStringValue:self.animations[row]];
        [cellView.checkBoxButton setTarget:self];
        [cellView.checkBoxButton setAction:@selector(cellAnimationChecked:)];
        [cellView.checkBoxButton setTag:row];
        
        NSString *path = [self pathForAnimationWithName:self.animations[row]];
        if ([KAAnimationParser shouldPreviewAnimationAtPath:path])
            [cellView.checkBoxButton setState:NSOnState];
        else
            [cellView.checkBoxButton setState:NSOffState];
        
        return cellView;
    }
    return nil;
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    if ([tableView isEqual:self.imagesTableView]) {
        return self.imagesNames.count;
    } else if ([tableView isEqual:self.animationsTableView]) {
        return self.animations.count;
    }
    return 0;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification{
    NSTableView *table = (NSTableView *)notification.object;
    if ([table isEqual:self.imagesTableView]) {
        if (table.numberOfSelectedRows == 0) {
            [self.removeImageButton setEnabled:NO];
            [self.addAnimationButton setEnabled:NO];
            [self.removeAnimationButton setEnabled:NO];
            [self.exportAnimationButton setEnabled:NO];
            [self.importAnimationButton setEnabled:NO];
        } else {
            [self.removeImageButton setEnabled:YES];
            [self.addAnimationButton setEnabled:YES];
            [self.importAnimationButton setEnabled:YES];
        }
    } else if ([table isEqual:self.animationsTableView]) {
        if (table.numberOfSelectedRows == 0) {
            [self.removeAnimationButton setEnabled:NO];
            [self.exportAnimationButton setEnabled:NO];
        } else {
            [self.removeAnimationButton setEnabled:YES];
            [self.exportAnimationButton setEnabled:YES];
        }
        return;
    }
    [self reFillAnimationsArray];
}

//- (BOOL)tableView:(NSTableView *)tableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard{
//    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:rowIndexes];
//    [pboard declareTypes:[NSArray arrayWithObject:kBasicTableViewDragAndDropDataType] owner:self];
//    [pboard setData:data forType:kBasicTableViewDragAndDropDataType];
//    return YES;
//}
//
//- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id<NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation{
//    return NSDragOperationAll;
//}
//
//- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id<NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation{
//    return YES;
//}

#pragma mark - Methods

- (void)cellAnimationChecked:(NSButton *)checkBox{
    NSString *path = [self pathForAnimationWithName:self.animations[checkBox.tag]];
    if (checkBox.state == NSOnState)
        [KAAnimationParser setShouldPreview:YES forAnimationAtPath:path];
    else if (checkBox.state == NSOffState)
        [KAAnimationParser setShouldPreview:NO forAnimationAtPath:path];
    NSInteger sIndex = self.animationsTableView.selectedRow;
    [self reFillAnimationsArray];
    NSIndexSet *set = [NSIndexSet indexSetWithIndex:sIndex];
    [self.animationsTableView selectRowIndexes:set byExtendingSelection:NO];
}

- (IBAction)stopIt:(NSButton *)sender {
    for (NSView *sv in self.iPhoneView.subviews)
        if ([sv isKindOfClass:[DraggableImageView class]])
            [sv.layer removeAllAnimations];
}

- (NSString *)pathForAnimationWithName:(NSString *)name{
    NSString *path = [[NSBundle mainBundle] bundlePath];
    path = [path stringByAppendingPathComponent:@"Images"];
    
    NSUInteger index = self.imagesTableView.selectedRow;
    
    if (index != -1 && index < self.imagesNames.count) {
        path = [path stringByAppendingPathComponent:self.imagesNames[index]];
        if ([self.animations containsObject:name]) {
            index = [self.animations indexOfObject:name];
            if (index != -1 && index < self.animations.count) {
                path = [path stringByAppendingPathComponent:self.animations[index]];
                path = [path stringByAppendingPathExtension:@"plist"];
                return path;
            }
        }
    }
    return nil;
}

@end
