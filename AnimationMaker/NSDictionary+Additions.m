//
//  NSDictionary+Additions.m
//  AnimationMaker
//
//  Created by Anton on 12.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings {
    const NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:self];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in self) {
        const id object = [self objectForKey:key];
        if (object == nul) {
            [replaced setObject:blank forKey:key];
        }
        else if ([object isKindOfClass: [NSDictionary class]]) {
            [replaced setObject:[(NSDictionary *)object dictionaryByReplacingNullsWithStrings] forKey:key];
        }
    }
    return [NSDictionary dictionaryWithDictionary:(NSDictionary *)replaced];
}

@end
