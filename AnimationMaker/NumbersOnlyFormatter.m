//
//  NumbersOnlyFormatter.m
//  AnimationMaker
//
//  Created by Anton on 12.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "NumbersOnlyFormatter.h"

@implementation NumbersOnlyFormatter

- (BOOL)isPartialStringValid:(NSString *)partialString
            newEditingString:(NSString **)newString errorDescription:(NSString **)error
/* this validates the field's value as each key is entered into textfield */
{
    /* validate the partial string */
    NSDecimalNumber *check; /* used to hold temp value */
    if ([self getObjectValue:&check forString:partialString
            errorDescription:error]) {return TRUE;}
    else
    {
        NSBeep();
        return FALSE;
    }
}

- (BOOL)getObjectValue:(id *)obj forString:(NSString *)string errorDescription:(NSString  **)error {
    
    double floatResult;
    NSScanner *scanner;
    BOOL returnValue = NO;
    
    scanner = [NSScanner scannerWithString: string];
    [scanner scanString: @"." intoString: NULL];    //ignore  return value
    if ([scanner scanDouble:&floatResult] && ([scanner isAtEnd])) {
        returnValue = YES;
        if (obj)
            *obj = @(floatResult);
    } else {
        if (error)
            *error = NSLocalizedString(@"Couldn’t convert  to float", @"Error converting");
    }
    return returnValue;
}

@end
