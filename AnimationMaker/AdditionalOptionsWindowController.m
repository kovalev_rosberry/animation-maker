//
//  AdditionalOptionsWindowController.m
//  AnimationMaker
//
//  Created by Anton on 09.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "AdditionalOptionsWindowController.h"
#import "NumbersOnlyFormatter.h"
#import "KAAnimationParserStrings.h"

@interface AdditionalOptionsWindowController ()

@property (strong) IBOutlet NSTextField *fillModeLabel;
@property (strong) IBOutlet NSMatrix *fillModeMatrix;
@property (strong) IBOutlet NSButton *autoreverseCheckBox;
@property (strong) IBOutlet NSButton *removeOnCompletionCheckBox;
@property (strong) IBOutlet NSButton *repeatCheckBox;
@property (strong) IBOutlet NSTextField *repeatCountLabel;
@property (strong) IBOutlet NSTextField *repeatDurationLabel;
@property (strong) IBOutlet NSTextField *repeatCountTextField;
@property (strong) IBOutlet NSTextField *repeatDurationTextField;
@property (strong) IBOutlet NSButton *repeatCountForeverCheckBox;
@property (strong) IBOutlet NSTextField *questionLabel;
@property (strong) IBOutlet NSButton *saveButton;
@property (strong) IBOutlet NSButton *cancelButton;
@property (strong) IBOutlet NSTextField *m34TextField;
@property (strong) IBOutlet NSButton *m34CheckBox;

@end

@implementation AdditionalOptionsWindowController

- (id)initWithWindowNibName:(NSString *)windowNibName andAnimationDictionary:(NSDictionary *)dictionary
{
    self = [super initWithWindowNibName:windowNibName];
    if (self) {
        _animationDictionary = [dictionary mutableCopy];
    }
    return self;
}

- (void)windowDidLoad{
    [super windowDidLoad];
    
    NumbersOnlyFormatter *formatter = [[NumbersOnlyFormatter alloc] init];
    [formatter setDecimalSeparator:@"."];
    [formatter setNumberStyle:NSNumberFormatterNoStyle];
    [formatter setAllowsFloats:YES];
    [formatter setMinimumFractionDigits:0];
    [formatter setMaximumFractionDigits:4];
    [formatter setPartialStringValidationEnabled:YES];
    [self.repeatCountTextField setFormatter:formatter];
    [self.repeatDurationTextField setFormatter:formatter];
    [self.m34TextField setFormatter:formatter];
    
    for (int i = 0; i < self.fillModeMatrix.cells.count; ++i)
        [self.fillModeMatrix setToolTip:[self tooltipForCellAtIndex:i] forCell:[self.fillModeMatrix cellAtRow:i column:0]];
    
    [self.questionLabel setHidden:YES];
    [self.autoreverseCheckBox setToolTip:@"Determines if the receiver plays in the reverse upon completion."];
    [self.removeOnCompletionCheckBox setToolTip:@"Determines if the animation is removed from the target layer’s animations upon completion.\n\nWhen YES, the animation is removed from the target layer’s animations once its active duration has passed. Defaults to YES."];
    [self.repeatCheckBox setToolTip:@"If checked animation will use repeat count and repeat duration values. Else this values will be ignored even if they was set."];
    [self.repeatCountLabel setToolTip:@"Determines the number of times the animation will repeat.\n\nMay be fractional. If the repeatCount is 0, it is ignored. Defaults to 0. If both repeatDuration and repeatCount are specified the behavior is undefined.\n\nChecking \"Forever\" checkbox will cause the animation to repeat forever."];
    [self.repeatDurationTextField setToolTip:@"Determines how many seconds the animation will repeat for.\n\nDefaults to 0. If the repeatDuration is 0, it is ignored. If both repeatDuration and repeatCount are specified the behavior is undefined."];
    [self.repeatCountForeverCheckBox setToolTip:@"Checking this checkbox will cause the animation to repeat forever and ignore \"Repeat count\" value."];
    [self.fillModeLabel setToolTip:@"Determines if the receiver’s presentation is frozen or removed once its active duration has completed.\n\nThe default is \"Remove\"."];
    [self.m34CheckBox setToolTip:@"m34 field of CATransform3D allow to simulate perspective effect. If you don't know what does it mean, you should find some information about this field."];
    
    [self refreshInterfaceWithNewData];
}

#pragma mark - Methods

- (NSString *)tooltipForCellAtIndex:(NSInteger)idx{
    switch (idx) {
        case 0:
            return @"The receiver remains visible in its final state when the animation is completed.";
            
        case 1:
            return @"The receiver clamps values before zero to zero when the animation is completed.";
            
        case 2:
            return @"The receiver clamps values at both ends of the object’s time space.";
            
        case 3:
            return @"The receiver is removed from the presentation when the animation is completed.";
            
        default:
            return @"";
    }
}

- (IBAction)fillModeMatrixClicked:(NSMatrix *)sender {
    [_animationDictionary setObject:@(sender.selectedRow) forKey:kAnimationFillModeKey];
    [self refreshInterfaceWithNewData];
}

- (IBAction)checkBoxClicked:(NSButton *)sender {
    NSString *key = nil;
    NSNumber *value = nil;
    if ([sender isEqualTo:self.autoreverseCheckBox]) {
        key = kAnimationAutoReverseKey;
        value = (sender.state == NSOnState) ? @YES : @NO;
    } else if ([sender isEqualTo:self.removeOnCompletionCheckBox]) {
        key = kAnimationRemoveOnCompletionKey;
        value = (sender.state == NSOnState) ? @YES : @NO;
    } else if ([sender isEqualTo:self.repeatCheckBox]) {
        key = kAnimationRepeatKey;
        value = (sender.state == NSOnState) ? @YES : @NO;
    } else if ([sender isEqualTo:self.repeatCountForeverCheckBox]) {
        key = kAnimationRepeatForeverKey;
        value = (sender.state == NSOnState) ? @YES : @NO;
    } else if ([sender isEqualTo:self.m34CheckBox]) {
        key = kAnimationShouldM34Key;
        value = (sender.state == NSOnState) ? @YES : @NO;
    }
    if (value && key)
        [_animationDictionary setObject:value forKey:key];
    [self refreshInterfaceWithNewData];
}

- (void)refreshInterfaceWithNewData{
    BOOL m34 = [_animationDictionary[kAnimationShouldM34Key] boolValue];
    if (m34)
        [self.m34CheckBox setState:NSOnState];
    
    [self.m34TextField setEnabled:m34];
    double m34Value = [_animationDictionary[kAnimationM34Key] doubleValue];
    [self.m34TextField setStringValue:[NSString stringWithFormat:@"%.2f", m34Value]];
    
    NSInteger selectedRButton = [[_animationDictionary objectForKey:kAnimationFillModeKey] doubleValue];
    if (selectedRButton > self.fillModeMatrix.cells.count || selectedRButton < 0)
        selectedRButton = self.fillModeMatrix.cells.count-1;
    [self.fillModeMatrix selectCellAtRow:selectedRButton column:0];
    
    BOOL aReverse = [[_animationDictionary objectForKey:kAnimationAutoReverseKey] boolValue];
    [self.autoreverseCheckBox setState:aReverse ? NSOnState : NSOffState];
    
    BOOL removeOnCompletion = [[_animationDictionary objectForKey:kAnimationRemoveOnCompletionKey] boolValue];
    [self.removeOnCompletionCheckBox setState:removeOnCompletion ? NSOnState : NSOffState];
    
    BOOL repeat = [[_animationDictionary objectForKey:kAnimationRepeatKey] boolValue];
    [self.repeatCheckBox setState:repeat ? NSOnState : NSOffState];
    
    [self.repeatCountForeverCheckBox setEnabled:repeat];
    [self.repeatDurationTextField setEnabled:repeat];
    
    double repeatCount = [[_animationDictionary objectForKey:kAnimationRepeatCountKey] doubleValue];
    [self.repeatCountTextField setStringValue:[NSString stringWithFormat:@"%.2f", repeatCount]];
    
    double repeatDuration = [[_animationDictionary objectForKey:kAnimationRepeatDurationKey] doubleValue];
    [self.repeatDurationTextField setStringValue:[NSString stringWithFormat:@"%.2f", repeatDuration]];
    
    BOOL repeatForever = [[_animationDictionary objectForKey:kAnimationRepeatForeverKey] boolValue];
    [self.repeatCountForeverCheckBox setState:repeatForever ? NSOnState : NSOffState];
    
    [self.repeatCountTextField setEnabled:(repeat && !repeatForever)];
}

- (void)controlTextDidChange:(NSNotification *)notification{
    if ([notification.object isEqualTo:self.m34TextField])
    {
        double value = [self.m34TextField.stringValue doubleValue];
        [_animationDictionary setObject:@(value) forKey:kAnimationM34Key];
    }
    else if ([notification.object isEqualTo:self.repeatCountTextField])
    {
        double value = [self.repeatCountTextField.stringValue doubleValue];
        [_animationDictionary setObject:@(value) forKey:kAnimationRepeatCountKey];
    }
    else if ([notification.object isEqualTo:self.repeatDurationTextField])
    {
        double value = [self.repeatDurationTextField.stringValue doubleValue];
        [_animationDictionary setObject:@(value) forKey:kAnimationRepeatDurationKey];
    }
}

- (IBAction)savePressed:(id)sender {
    [NSApp endSheet:self.window returnCode:NSModalResponseOK];
}

- (IBAction)cancelPressed:(id)sender {
    [NSApp endSheet:self.window returnCode:NSModalResponseCancel];
}

@end
