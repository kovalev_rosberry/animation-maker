//
//  AdditionalOptionsWindowController.h
//  AnimationMaker
//
//  Created by Anton on 09.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AdditionalOptionsWindowController : NSWindowController

- (id)initWithWindowNibName:(NSString *)windowNibName andAnimationDictionary:(NSDictionary *)dictionary;

@property (nonatomic, strong, readonly) NSMutableDictionary *animationDictionary;

@end
