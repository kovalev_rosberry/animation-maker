//
//  NSApplication+Additions.m
//  AnimationMaker
//
//  Created by Anton on 07.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "NSApplication+Additions.h"

@implementation NSApplication (Additions)


- (void)beginSheet:(NSWindow *)sheet modalForWindow:(NSWindow *)docWindow didEndBlock:(ModalSheetEndBlock)block
{
    sheet.modalSheetEndBlock = block;
    [self beginSheet:sheet
      modalForWindow:docWindow
       modalDelegate:self
      didEndSelector:@selector(my_blockSheetDidEnd:returnCode:contextInfo:)
         contextInfo:nil];
}

- (void)my_blockSheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo
{
    if (sheet.modalSheetEndBlock)
        sheet.modalSheetEndBlock(returnCode);
    [sheet orderOut:nil];
}

@end
