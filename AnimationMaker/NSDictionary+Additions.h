//
//  NSDictionary+Additions.h
//  AnimationMaker
//
//  Created by Anton on 12.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings;

@end
