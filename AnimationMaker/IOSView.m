//
//  IOSView.m
//  AnimationMaker
//
//  Created by Anton on 30.04.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "IOSView.h"

@interface IOSView (){
    CGPoint lastDragLocation;
}

@end

@implementation IOSView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

- (BOOL)isFlipped{
    return YES;
}

@end
