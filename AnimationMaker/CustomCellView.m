//
//  CustomCellView.m
//  AnimationMaker
//
//  Created by Anton on 18.07.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "CustomCellView.h"

@implementation CustomCellView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

@end
