//
//  CreateOrEditAnimationWindowController.h
//  AnimationMaker
//
//  Created by Anton on 08.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CreateOrEditAnimationWindowController : NSWindowController

@property (nonatomic, strong) NSString *pathToPlist;

@end
 