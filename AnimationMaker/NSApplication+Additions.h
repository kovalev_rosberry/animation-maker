//
//  NSApplication+Additions.h
//  AnimationMaker
//
//  Created by Anton on 07.05.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NSWindow+Utils.h"

#define fequal(a,b) (fabs((a)-(b)) < FLT_EPSILON)

@interface NSApplication (Additions)

- (void)beginSheet:(NSWindow *)sheet modalForWindow:(NSWindow *)docWindow didEndBlock:(void (^)(NSInteger returnCode))block;

@end
